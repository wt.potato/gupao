package com.wtd.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author wt.d
 * @date 15:40 2018/7/20
 */
public class JdbcConn {

    public static void main(String[] args) {
        try {
            //&allowMultiQueries=true
            String url = "jdbc:mysql://192.168.139.137:3306/activemq?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&relaxAutoCommit=true";
            String user = "root";
            String password = "system";
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(url, user, password);
            System.out.println("连接测试：" + connection);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
