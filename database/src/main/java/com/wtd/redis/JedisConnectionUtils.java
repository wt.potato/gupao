package com.wtd.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author wt.d
 * @date 16:32 2018/9/29
 */
public class JedisConnectionUtils {

    private static JedisPool pool = null;

    static {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(100);
        pool = new JedisPool(jedisPoolConfig,"192.168.139.129", 7001);
    }

    public static Jedis getJedis() {
        return pool.getResource();
    }
}
