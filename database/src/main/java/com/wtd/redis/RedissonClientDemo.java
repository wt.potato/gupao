package com.wtd.redis;

import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;

/**
 * @author wt.d
 * @date 15:29 2018/9/29
 */
public class RedissonClientDemo {

    public static void main(String[] args) {
        Config config = new Config();
        config.useClusterServers().addNodeAddress(
                "redis://192.168.139.129:7001",
                "redis://192.168.139.130:7004",
                "redis://192.168.139.131:7007");
        RedissonClient redissonClient = Redisson.create(config);
//        redissonClient.getLock("");//分布式锁
        RBucket<String> bucket = redissonClient.getBucket("name");
        System.out.println(bucket.get());
        redissonClient.shutdown();
    }
}
