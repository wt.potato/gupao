package com.wtd.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.UUID;

/**
 * @author wt.d
 * @date 16:36 2018/9/29
 */
public class DistributedLock {

    /**
     * 获取锁
     * @param lockName 锁的名称
     * @param acquireTimeout 获取锁的超时时间
     * @param lockTimeout 所本身的过期时间
     * @return
     */
    public String acquireLock(String lockName, long acquireTimeout, long lockTimeout){
        String indentifier = UUID.randomUUID().toString();//保证释放锁的时候是同一个持有锁的人
        String lockKey = "lock:" + lockName;
        int lockExpire = (int) (lockTimeout / 1000);
        Jedis jedis = null;
        try {
            jedis = JedisConnectionUtils.getJedis();
            long end = System.currentTimeMillis() + acquireTimeout;
            while (System.currentTimeMillis() < end) {
                if (jedis.setnx(lockKey, indentifier) == 1) { //设置值成功
                    jedis.expire(lockKey, lockExpire); //设置超时时间
                    return indentifier; //获得锁成功
                }

                if (jedis.setnx(lockKey, indentifier) == -1) {
                    jedis.expire(lockKey, lockExpire); //设置超时时间
                }

                try {
                    //等待片刻后进行获取锁的重试
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            //回收
            jedis.close();
        }
        return null;
    }

    /**
     * 释放锁
     * @param lockName 锁的名称
     * @param identifier 锁id
     * @return
     */
    public boolean releaseLockWithLua(String lockName, String identifier){
        System.out.println(lockName + "开始释放锁:" + identifier);
        String lockKey = "lock:" + lockName;
        Jedis jedis = null;
        boolean isRelease = false;
        try {
            jedis = JedisConnectionUtils.getJedis();
            String lua = "if redis.call(\"get\",KEYS[1])==ARGV[1] then " +
                    "return redis.call(\"del\",KEYS[1]) " +
                    "else return 0 end";
            long rs = (long) jedis.eval(lua, 1, new String[]{lockKey, identifier});
            if (rs > 0) {
                isRelease = true;
            }
        } finally {
            jedis.close();
        }
        return isRelease;
    }

    /**
     * 释放锁
     * @param lockName 锁的名称
     * @param identifier 锁id
     * @return
     */
    public boolean releaseLock(String lockName, String identifier){
        System.out.println(lockName + "开始释放锁:" + identifier);

        String lockKey = "lock:" + lockName;
        Jedis jedis = null;
        boolean isRelease = false;
        try {
            jedis = JedisConnectionUtils.getJedis();
            while (true) {
                jedis.watch(lockKey);
                if (identifier.equals(jedis.get(lockKey))){
                    Transaction transaction = jedis.multi();
                    transaction.del(lockKey);
                    if (transaction.exec().isEmpty()) {
                        continue;
                    }
                    isRelease = true;
                }
                jedis.unwatch();
                break;
            }
        } finally {
            jedis.close();
        }
        return isRelease;
    }
}
