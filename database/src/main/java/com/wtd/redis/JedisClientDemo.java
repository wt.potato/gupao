package com.wtd.redis;


import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.*;

import java.util.HashSet;
import java.util.Set;

/**
 * @author wt.d
 * @date 14:33 2018/9/29
 */
public class JedisClientDemo {

    public static void main(String[] args) {
        //哨兵 sentinel，由哨兵获取master地址，初始化连接池
//        String masterName = null;
//        Set<String> sentinels = new HashSet<>();
//        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
//        JedisSentinelPool jedisSentinelPool = new JedisSentinelPool(masterName, sentinels, jedisPoolConfig);
        //redis集群
//        Set<HostAndPort> hostAndPorts = new HashSet<>();
//        hostAndPorts.add(new HostAndPort("192.168.139.135", 6379));
//        hostAndPorts.add(new HostAndPort("192.168.139.130", 7004));
//        hostAndPorts.add(new HostAndPort("192.168.139.131", 7007));
//        JedisCluster jedisCluster = new JedisCluster(hostAndPorts);
//        jedisCluster.set("name","wtd");
//        jedisCluster.get("name");
        //单机节点
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(100);
        JedisPool pool = new JedisPool(poolConfig,"192.168.139.135",6379);
        Jedis jedis = pool.getResource();
        System.out.println("gray->" + jedis.get("gray"));
    }
}
