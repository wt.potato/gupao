package com.wtd.redis;

import static org.junit.Assert.*;

/**
 * @author wt.d
 * @date 16:56 2018/9/29
 */
public class DistributedLockTest extends Thread {
    static DistributedLock distributedLock = null;
    @Override
    public void run() {
        while (true) {
            distributedLock = new DistributedLock();
            String rs = distributedLock.acquireLock("updateOrder", 2000, 5000);
            if (rs != null) {
                System.out.println(Thread.currentThread().getName() + "-> 成功获得锁:" + rs);
                try {
                    Thread.sleep(1000);
                    distributedLock.releaseLockWithLua("updateOrder", rs);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void main(String[] args) {
        DistributedLockTest distributedLockTest = new DistributedLockTest();
        for (int i = 0; i < 10; i++) {
            new Thread(distributedLockTest,"tName:" + i).start();
        }
    }
}