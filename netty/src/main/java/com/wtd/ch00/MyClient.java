package com.wtd.ch00;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wt.d
 * @date 11:26 2018/11/14
 */
@Slf4j
public class MyClient extends ChannelInboundHandlerAdapter{

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        log.info("client receive message:" + msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.info("get client exception:" + cause.getMessage());
    }
}
