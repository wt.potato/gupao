package com.wtd.biochat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author wt.d
 * @date 15:21 2018/11/6
 */
public class Client {

    public static void main(String[] args) throws IOException {
        System.out.println("请输入名称:");
        String name = new BufferedReader(new InputStreamReader(System.in)).readLine();
        if (name.equals("")) return;
        Socket socket = new Socket("127.0.0.1", 9999);
        new Thread(new Send(socket, name)).start();
        new Thread(new Receive(socket)).start();
    }
}
