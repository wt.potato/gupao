package com.wtd.biochat;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * @author wt.d
 * @date 15:07 2018/11/6
 */
public class Receive implements Runnable{

    private DataInputStream dis;
    private boolean isRunning = true;

    public Receive(Socket socket) {
        try {
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String receive(){
        String message = "";
        try {
            message = dis.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }

    @Override
    public void run() {
        while (isRunning) {
            System.out.println("->"+receive());
        }
    }
}
