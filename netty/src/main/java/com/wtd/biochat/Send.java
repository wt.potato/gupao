package com.wtd.biochat;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author wt.d
 * @date 15:07 2018/11/6
 */
public class Send implements Runnable{

    private BufferedReader reader;
    private DataOutputStream dos;
    private boolean isRunning = true;
    private String name;

    public Send() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public Send(Socket socket, String name) {
        this();
        this.name = name;
        try {
            dos = new DataOutputStream(socket.getOutputStream());
            send(this.name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String message){
        try {
            if (message != null && !message.equals("")) {
                dos.writeUTF(message);
                dos.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getMessageFromConsole() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void run() {
        while (isRunning) {
            send(getMessageFromConsole());
        }
    }
}
