package com.wtd.biochat;

import java.io.Closeable;

/**
 * @author wt.d
 * @date 17:24 2018/11/2
 */
public class CloseUtil {
    public static void closeAll(Closeable ...io) {
        for (Closeable temp : io) {
            try {
                if (null != temp) {
                    temp.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
