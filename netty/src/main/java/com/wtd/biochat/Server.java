package com.wtd.biochat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wt.d
 * @date 14:30 2018/11/6
 */
public class Server {

    private List<MyChannel> mychannels = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        new Server().start();
    }

    public void start() throws IOException {
        ServerSocket serverSocket = new ServerSocket(9999);
        while (true) {
            Socket socket = serverSocket.accept();
            MyChannel channel = new MyChannel(socket);
            mychannels.add(channel);
            System.out.println("连接个数:" + mychannels.size());
            new Thread(channel).start();
        }
    }

    private class MyChannel implements Runnable {

        private DataInputStream dis;
        private DataOutputStream dos;
        private boolean isRunning = true;
        private String name;

        public MyChannel(Socket socket) {
            try {
                dis = new DataInputStream(socket.getInputStream());
                dos = new DataOutputStream(socket.getOutputStream());
                this.name = dis.readUTF();
                this.send("欢迎您进入聊天室");
                sendOthers(this.name + "进入了聊天室",true);
            } catch (IOException e) {
                e.printStackTrace();
                CloseUtil.closeAll(dis,dos);
                isRunning =false;
            }
        }

        /**
         * 读取消息
         * @return
         */
        private String receive(){
            String message = null;
            try {
                message = dis.readUTF();
            } catch (IOException e) {
                e.printStackTrace();
                CloseUtil.closeAll(dis);
                isRunning = false;
                mychannels.remove(this); //移除自身
            }
            return message;
        }

        /**
         * 发送数据
         * @param message
         */
        private void send(String message) {
            if (message == null || message.equals("")) return;
            try {
                dos.writeUTF(message);
                dos.flush();
            } catch (IOException e) {
                e.printStackTrace();
                CloseUtil.closeAll(dos);
                isRunning =false;
                mychannels.remove(this); //移除自身
            }
        }

        /**
         * 发送给其它客户端
         * @param message
         * @param sys
         */
        private void sendOthers(String message, boolean sys) {
            if (message.startsWith("@") && message.indexOf(":") > -1) {
                String name = message.substring(1, message.indexOf(":"));
                String content = message.substring(message.indexOf(":") + 1);
                mychannels.forEach(obj ->{
                    if (obj.name.equals(name)) obj.send(this.name + "对你说:" + content);
                });
            } else {
                mychannels.forEach(obj ->{
                    if (obj == this) return;
                    if (sys) {
                        obj.send("系统消息:" + message);
                    } else {
                        obj.send(this.name + "对大家说:" + message);
                    }
                });
            }
        }

        @Override
        public void run() {
            while (isRunning) {
                sendOthers(receive(), false);
            }
        }
    }
}
