package com.wtd.bio;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author wt.d
 * @date 11:25 2018/11/2
 */
public class Client {
    private static int DEFAULT_SERVERR_PORT = 7777;
    private static String DEFAULT_SERVERR_IP = "127.0.0.1";

    public static void send(String expression){
        System.out.println("算术表达式为:" + expression);
        Socket socket = null;
        BufferedReader reader = null;
        PrintWriter writer = null;
        try {
            socket = new Socket(DEFAULT_SERVERR_IP, DEFAULT_SERVERR_PORT);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(),true);
            writer.println(expression);
            System.out.println("_结果为:" + reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                reader = null;
            }
            if (writer != null) {
                writer.close();
                writer = null;
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                socket = null;
            }
        }
    }
}
