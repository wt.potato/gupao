package com.wtd.bio;

import java.util.Arrays;

/**
 * @author wt.d
 * @date 10:46 2018/11/2
 */
public class Calculator {
    private static char[] op = {'+', '-', '*', '/'};
    public static String call(String expression) {
        int index = 0;
        int x = 0, y = 0;
        for (char chr:op) {
           if ((index = expression.indexOf(chr)) != -1) {
               x = Integer.parseInt(expression.substring(0, index));
               y = Integer.parseInt(expression.substring(index + 1));
           }
        }
        return String.valueOf(x + y);
    }
}
