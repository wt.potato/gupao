package com.wtd.bio;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * @author wt.d
 * @date 16:32 2018/11/1
 */
public class Server {

    private static int DEFAULT_PORT = 7777;

    private static ServerSocket serverSocket;

    public static void start() throws IOException {
        start(DEFAULT_PORT);
    }

    private static void start(int port) throws IOException {
        if (serverSocket != null) return;

        try {
            serverSocket = new ServerSocket(port);
            System.out.println("服务端已启动,端口:" + port);
            while (true) {
                Socket socket = serverSocket.accept();
                new Thread(()->{
                    new ServerHandler(socket).run();
                }).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                serverSocket.close();
                serverSocket = null;
            }
        }
    }
}
