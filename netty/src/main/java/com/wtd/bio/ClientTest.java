package com.wtd.bio;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Random;

/**
 * @author wt.d
 * @date 13:39 2018/11/2
 */
public class ClientTest {
    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            try {
                Server.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        Thread.sleep(100);

        char[] op = {'+', '-', '*', '/'};

        final Random random = new Random(System.currentTimeMillis());
        new Thread(()->{
            while (true) {
                String expression = random.nextInt(10) + "" + op[random.nextInt(4)] + "" +
                        (random.nextInt(10) + 1);
                Client.send(expression);
                try {
                    Thread.sleep(1000 * 2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
