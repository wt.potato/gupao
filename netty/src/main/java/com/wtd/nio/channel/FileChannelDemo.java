package com.wtd.nio.channel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author wt.d
 * @date 10:13 2018/11/7
 */
public class FileChannelDemo {

    public static void main(String[] args) {
        //创建文件，想文件中写入数据
        try {
            File file = new File("C:/install-work/nio_utf8.data");
            if (!file.exists()) {
                file.createNewFile();
            }

            //根据文件输出流创建于这个文件相关的通道
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            FileChannel fileChannel = fileOutputStream.getChannel();

            //创建ButeBuffer对象，positioiin = 0, limit = 64
            ByteBuffer byteBuffer = ByteBuffer.allocate(64);

            System.out.println("position->" + byteBuffer.position() + ",limit->" + byteBuffer.limit() + ",capacity->" + byteBuffer.capacity());
            //箱ByteBuffer中放入字符串UTF-8的字节，position =17, limit = 64
            byteBuffer.put("Hello,World 123 \n".getBytes("UTF-8"));
            System.out.println("position->" + byteBuffer.position() + ",limit->" + byteBuffer.limit() + ",capacity->" + byteBuffer.capacity());

            //flip方法 position = 0, limit = 17
            byteBuffer.flip();
            System.out.println("position->" + byteBuffer.position() + ",limit->" + byteBuffer.limit() + ",capacity->" + byteBuffer.capacity());

            //write方法使得ByteBuffer的position到limit中的元素写入通道中
            fileChannel.write(byteBuffer);

            //clear方法使得position = 0， limit = 64
            byteBuffer.clear();
            System.out.println("position->" + byteBuffer.position() + ",limit->" + byteBuffer.limit() + ",capacity->" + byteBuffer.capacity());

            //以下代码同理
            byteBuffer.put("你好，世界 456".getBytes("UTF-8"));
            byteBuffer.flip();

            fileChannel.write(byteBuffer);
            byteBuffer.clear();

            fileOutputStream.close();
            fileChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
