package com.wtd.nio.buffer;

import java.nio.ByteBuffer;

/**
 * 自定义Buffer类中包含读缓冲区和写缓冲区，用于注册通道时附加对象
 * @author wt.d
 * @date 09:43 2018/11/7
 */
public class Buffers {

    ByteBuffer readBuffer;
    ByteBuffer writeBuffer;

    public Buffers(int readCapacity, int writeCapacity) {
        this.readBuffer = ByteBuffer.allocate(readCapacity);
        this.writeBuffer = ByteBuffer.allocate(writeCapacity);
    }

    public ByteBuffer getReadBuffer() {
        return readBuffer;
    }

    public ByteBuffer getWriteBuffer() {
        return writeBuffer;
    }
}
