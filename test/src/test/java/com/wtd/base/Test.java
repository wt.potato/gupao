package com.wtd.base;

import java.util.*;

/**
 * @author wt.d
 * @date 16:22 2018/12/6
 */
public class Test {

    public static void main(String[] args) {
        int allNum = 2;

        Map<String, Integer> map = new HashMap<>();
        map.put("A", 0);
        map.put("B", 0);
        map.put("C", 0);
        map.put("d", 10);
//        map.put("E", 2);
//        map.put("D", 7);

        List<Map.Entry<String,Integer>> list = new ArrayList<>(map.entrySet());
        //升序排序
        Collections.sort(list, Comparator.comparing(Map.Entry::getValue));

        List<Map.Entry<String,Integer>> oldlist = new ArrayList<>();
        oldlist.addAll(list);

        for(int i = (list.size()-1); i > 0; i--){
            if(list.get(i).getValue() > allNum){
                list.remove(i);
            }else{
                int sum = getCount(list.get(i).getValue(),i,list);
                if(sum > allNum){
                    list.remove(i);
                }else{

                    int midNum ;
                    if(list.size() == oldlist.size()){
                        midNum = list.get(i).getValue();
                    }else{
                        midNum = (list.get(i).getValue()+oldlist.get(i+1).getValue())/2;
                    }
                    midNum = getMidNum(midNum,i,list,oldlist,allNum);

                    int surplus = 0;
                    StringBuilder sb = new StringBuilder();
                    for(int j = 0; j<= i; j++){
                        surplus += midNum - list.get(j).getValue();
                        sb.append(list.get(j).getKey()+",");
                        System.out.println(list.get(j).getKey()+"分配："+(midNum - list.get(j).getValue()));
                    }
                    System.out.println("剩余"+(allNum-surplus)+"个，随机分配给"+sb.toString()+"中的"+(allNum-surplus)+"个");
                    break;
                }
            }

        }

    }

    public static int getMidNum(int midNum, int i, List<Map.Entry<String,Integer>> list,List<Map.Entry<String,Integer>> oldlist, int allNum){
        int sum = getCount(midNum,i,list);
        System.out.println(midNum+"---------"+sum+"---"+(midNum * (i + 1)));
//        if(midNum == 0 || midNum == sum || sum == midNum * (i + 1)) return allNum / (i + 1);
        if(midNum == 0 || sum == midNum * (i + 1)) return allNum / (i + 1);
        if(sum > allNum){
            midNum = (list.get(i).getValue()+midNum)/2;
            return getMidNum(midNum,i,list,oldlist,allNum);
        }else if((allNum-sum)>=list.size()){
            System.out.println((allNum-sum)+"----------"+oldlist.size() + "---------" + (i + 1)+"-------"+midNum);
            midNum = getNum(oldlist, i, midNum);
            return getMidNum(midNum,i,list,oldlist,allNum);
        }else{
            return midNum;
        }
    }
    private static int getNum(List<Map.Entry<String,Integer>> oldlist, int i, int midNum){
        if(oldlist.size() > i + 1) {
            return (oldlist.get(i+1).getValue()+midNum)/2;
        }
        return midNum + 1;
    }

    public static int getCount(int midNum, int i,List<Map.Entry<String,Integer>> list){
        int sum = 0;
        for(int j = 0; j<= i; j++){
            sum += (midNum - list.get(j).getValue());
        }
        return sum;
    }
}
