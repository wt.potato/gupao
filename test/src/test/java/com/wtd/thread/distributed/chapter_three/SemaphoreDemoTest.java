package com.wtd.thread.distributed.chapter_three;

import org.junit.Test;

import java.util.concurrent.Semaphore;

import static org.junit.Assert.*;

public class SemaphoreDemoTest {
    @Test
    public void method1() throws Exception {
       SemaphoreDemo semaphoreDemo = new SemaphoreDemo();
        for (int i = 0; i < 5; i++) {
            semaphoreDemo.method1(i);
        }
    }

}