package com.wtd.thread.distributed.chapter_three;

import org.junit.Test;

/**
 * @author wt.d
 * @date 16:13 2018/8/24
 */
public class ThreadPoolTest {

    @Test
    public void test1(){
        new ThreadPool().test1();
    }

    @Test
    public void test2(){
        new ThreadPool().test2();
    }
}
