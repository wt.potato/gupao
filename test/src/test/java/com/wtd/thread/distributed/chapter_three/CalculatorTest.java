package com.wtd.thread.distributed.chapter_three;

import org.junit.Test;
import sun.reflect.generics.tree.VoidDescriptor;

import java.lang.reflect.Proxy;

import static org.junit.Assert.*;

public class CalculatorTest {

    /**
     * 静态代理测试
     */
    @Test
    public void test1(){
        CalculatorProxy calculatorProxy = new CalculatorProxy(new CalculatorImpl());
        System.out.println(calculatorProxy.add(1, 2));
    }

    /**
     * 动态代理测试
     */
    @Test
    public void test2(){
        Calculator calculator = new CalculatorImpl();
        LogHandler lh = new LogHandler(calculator);
        Calculator proxy = (Calculator) Proxy.newProxyInstance(calculator.getClass().getClassLoader(), calculator.getClass().getInterfaces(), lh);
        proxy.add(1, 2);
    }

    @Test
    public void test3(){
        Calculator proxy = (Calculator) new LogHandlerTWO().getInstance(new CalculatorImpl());
        proxy.add(1, 2);
    }

    @Test
    public void test4(){
        System.out.println(Object.class);
    }
}