package com.wtd.thread.distributed.chapter_one;

import java.util.concurrent.CountDownLatch;

/**
 * @author wt.d
 * @date 10:19 2018/8/22
 */
public class TestClass {

    public static void main(String[] args) {
       final TestClass1 testClass1 = new TestClass1();
       final TestClass2 testClass2 = new TestClass2();
        CountDownLatch countDownLatch = new CountDownLatch(50);
//        for (int i = 0; i < 50; i++) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        countDownLatch.await();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    testClass2.add("1");
//                }
//            },"HashMap").start();
//            countDownLatch.countDown();
//        }

        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                try {
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                testClass2.add("1");
            },"ConcurrentHashMap").start();
            countDownLatch.countDown();
        }
        try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        testClass2.mapSize();

    }
}
