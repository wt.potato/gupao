package com.wtd.juc;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author wt.d
 * @date 10:45 2018/9/26
 */
public class LockDemoTest {
    @Test
    public void test() throws Exception {
        final LockDemo lockDemo = new LockDemo();
        Thread t1 = new Thread(()->{
            try {
                System.out.println("take->" + lockDemo.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"take");
        Thread t2 = new Thread(()->{
            try {
                lockDemo.put(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"put");
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }


}