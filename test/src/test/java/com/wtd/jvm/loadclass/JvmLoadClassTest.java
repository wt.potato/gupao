package com.wtd.jvm.loadclass;

import org.junit.Test;

public class JvmLoadClassTest {

    @Test
    public void method1(){
        JvmLoadClass jvmLoadClass = new JvmLoadClass();
        jvmLoadClass.method1();
    }

    @Test
    public void method2(){
        JvmLoadClass.method2();
    }
}
