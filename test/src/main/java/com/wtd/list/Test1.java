package com.wtd.list;

import java.util.List;

/**
 * @author wt.d
 * @date 15:37 2018/9/27
 */
public class Test1 {

    /**
     * 遍历链表
     * @param head
     */
    public static void traverse(ListNode head){
        while (head != null) {
            System.out.print(head.value + "->");
            head = head.next;
        }
        System.out.println();
    }

    /**
     * 反转链表
     * 0(n) 0(1)
     * @param head
     * @return
     */
    public static ListNode reverseList(ListNode head){
        ListNode pre = null;//当前节点的上一个节点
        ListNode next = null;//当前节点的下一个节点
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    /**
     * 取中间节点(偶数个数取得中间节点是前面那个)
     * @param head
     * @return
     */
    public static ListNode getMid(ListNode head){
        if (head == null) {
            return head;
        }
        ListNode fast = head;
        ListNode slow = head;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    public static void main(String[] args) {
        ListNode node0 = new ListNode(0);
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);
        ListNode node7 = new ListNode(7);
        ListNode node8 = new ListNode(8);
        ListNode node9 = new ListNode(9);
        node0.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;
        node7.next = node8;
        node8.next = node9;
        node9.next = null;
        System.out.println("取中间节点->" + getMid(node0).value);
        ListNode listNode = reverseList(node0);
        while (listNode != null) {
            System.out.print(listNode.value + "->");
            listNode = listNode.next;
        }
    }
}
