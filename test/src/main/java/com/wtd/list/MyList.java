package com.wtd.list;

import jdk.nashorn.internal.runtime.regexp.joni.constants.Traverse;
import sun.swing.plaf.synth.DefaultSynthStyle;

import java.util.List;

/**
 * @author wt.d
 * @date 11:10 2018/9/27
 */
public class MyList {

    /**
     * 头结点插入
     * @param head
     * @param newHead
     */
    public static void headInsert(ListNode head,ListNode newHead){
        ListNode old = head;
        head = newHead;
        head.next = old;
    }

    /**
     * 尾节点插入
     * @param tail
     * @param newTail
     */
    public static void tailInsert(ListNode tail, ListNode newTail){
        ListNode old = tail;
        tail = newTail;
        tail.next = null;
        old.next = tail;
    }

    /**
     * 遍历
     * @param head
     */
    public static void traverse(ListNode head){
        while (head != null) {
            System.out.print(head.value + "->");
            head = head.next;
        }
        System.out.println();
    }

    /**
     * 查找
     * @param head
     * @param value
     */
    public static int find(ListNode head, int value){
        int index = -1;
        int count = 0;
        while (head != null) {
            if (head.value == value){
                index = count;
                return index;
            }
            count++;
            head = head.next;
        }
        return index;
    }

    /**
     * 插入
     * @param p
     * @param s
     */
    public static void insert(ListNode p, ListNode s){
        ListNode next = p.next;
        p.next = s;
        s.next = next;
    }

    /**
     * 删除
     * @param head
     * @param q
     */
    public static void delete(ListNode head, ListNode q){
        if (q != null && q.next != null) {
            ListNode p = q.next;
            q.value = p.value;
            //删除q.next
            q.next = p.next;
            p = null;
        }
        //删除最后一个元素情况
        if (q.next == null) {
            while (head != null) {
                if (head.next != null && head.next == q) {
                    head.next = null;
                    break;
                }
                head = head.next;
            }
        }
    }

    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        node1.next = node2;
        node2.next = node3;
        node3.next = null;
        //遍历
        traverse(node1);
        //头结点插入
        ListNode newhead = new ListNode(0);
        headInsert(node1, newhead);
        traverse(newhead);
        //尾节点插入
        ListNode newTail = new ListNode(4);
        tailInsert(node3, newTail);
        traverse(newhead);
        //查找
        System.out.println(find(newhead,3));
        //插入
        ListNode node = new ListNode(5);
        insert(node3, node);
        traverse(newhead);
        //删除
        delete(newhead, node3);
        traverse(newhead);

    }
}
