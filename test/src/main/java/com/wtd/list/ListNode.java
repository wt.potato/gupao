package com.wtd.list;

/**
 * @author wt.d
 * @date 14:15 2018/9/27
 */
public class ListNode {

    public int value;
    public ListNode next;

    public ListNode(int value) {
        this.value = value;
    }
}
