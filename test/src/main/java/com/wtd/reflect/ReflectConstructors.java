package com.wtd.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Currency;

/**
 * @author wt.d
 * @date 13:58 2018/6/21
 */
public class ReflectConstructors {
    public static void main(String[] args) {
        //构造方法获取
        Class<?> clazz = null;
        try {
            clazz = Class.forName("com.wtd.reflect.Student");
            System.out.println("-------------------所有公有构造方法-------------------");
            Constructor[] constructors =  clazz.getConstructors();
            for (Constructor constructor : constructors){
                System.out.println(constructor);
            }
            System.out.println("-------------------所有构造方法-------------------");
            constructors =  clazz.getDeclaredConstructors();
            for (Constructor constructor : constructors){
                System.out.println(constructor);
            }
            System.out.println("-------------------获取公有，无参构造方法-------------------");
            Constructor constructor =  clazz.getConstructor(null);
            System.out.println(constructor);
            Object object = constructor.newInstance();

            System.out.println("-------------------获取所有构造方法，并调用-------------------");
            constructor =  clazz.getDeclaredConstructor(String.class);
            System.out.println(constructor);
            constructor.setAccessible(true);//暴力访问，忽略修饰符
            object = constructor.newInstance("张三");
            Student student = (Student) object;
            System.out.println("name:" + student.getName());

            constructor =  clazz.getDeclaredConstructor(String.class, String.class, Integer.class);
            System.out.println(constructor);
            constructor.setAccessible(true);//暴力访问，忽略修饰符
            object = constructor.newInstance("李四", "男", 20);
            student = (Student) object;
            System.out.println("name:" + student.getName() + ", sex:" + student.getSex() + ", age:" + student.getAge());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
