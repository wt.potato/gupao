package com.wtd.reflect;

import javax.sound.midi.Soundbank;

/**
 * @author wt.d
 * @date 13:49 2018/6/21
 */
public class ReflectClass {

    public static void main(String[] args) {
        //获取class字节码文件的方式
        Student student = new Student();
        Class<?> clazz = student.getClass();
        System.out.println("第一种：" + clazz.getName());

        Class<?> clazz2 = Student.class;
        System.out.println("第二种：" + clazz2.getName());
        System.out.println("判断是否相等：" + (clazz == clazz2));

        Class<?> clazz3 = null;
        try {
            clazz3 = Class.forName("com.wtd.reflect.Student");
            System.out.println("第三种：" + clazz3.getName());
            System.out.println("判断是否相等：" + (clazz2 == clazz3));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
