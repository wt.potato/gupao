package com.wtd.reflect;

/**
 * @author wt.d
 * @date 15:09 2018/6/21
 */
public class SuperClass {

    private String teacher;

    public void  sayHello(String str){
        System.out.println("Hello," + str);
    }

}
