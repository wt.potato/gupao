package com.wtd.reflect;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author wt.d
 * @date 15:32 2018/6/21
 */
public class ReflectConf {

    public static void main(String[] args) throws Exception {
        String cls = new ReflectConf().getValue("className");
        String mth = new ReflectConf().getValue("methodName");
        Class<?> clazz = Class.forName(cls);
        Method method = clazz.getMethod(mth);
        method.invoke(clazz.getConstructor().newInstance());

        //越过泛型检查
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        clazz = list.getClass();
        method = clazz.getMethod("add", Object.class);
        method.invoke(list, 100);

        for (Object o : list) {
            System.out.println(o);
        }
    }

    public String getValue(String key) throws IOException {
        Properties conf = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("reflect.properties");
        conf.load(inputStream);
        inputStream.close();
        return conf.getProperty(key);
    }
}
