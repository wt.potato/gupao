package com.wtd.reflect;

import java.lang.reflect.Field;

/**
 * @author wt.d
 * @date 11:42 2018/12/10
 */
public class TeacherReflect {
    public static void main(String[] args) {
        String a = "{\"errorCode\":\"0000\",\"errorMsg\":\"成功\"}";
        //构造方法获取
        Class<?> clazz = null;
        try {
            clazz = Teacher.class;
            Field[] fields = clazz.getFields();
            for (Field field : fields) {
                System.out.println(field.getName());
                System.out.println(field.get(clazz));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
