package com.wtd.reflect;

import java.lang.reflect.Method;

/**
 * @author wt.d
 * @date 13:58 2018/6/21
 */
public class ReflectMain {
    public static void main(String[] args) {
        //构造方法获取
        Class<?> clazz = null;
        try {
            clazz = Class.forName("com.wtd.reflect.Student");
           Method method = clazz.getMethod("main", String[].class);
           method.invoke(null, (Object) new String[]{"a","b"});
           method.invoke(null, new Object[]{new String[]{"a","b"}});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
