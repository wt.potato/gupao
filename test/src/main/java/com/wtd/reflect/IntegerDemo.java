package com.wtd.reflect;

import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;

/**
 *  引用类型，基本类型  Integer是int的包装类。 int 基本类型， Integer 为引用类型
 * 自动装箱与拆箱
 * Integer 值 -128到127之间的缓存
 * 反射，通过反射去修改 private final 变量
 * @author wt.d
 * @date 17:22 2018/9/19
 */
public class IntegerDemo {

    public static void main(String[] args) throws Exception {
        Integer a = 1, b = 2;//public static Integer valueOf(int i)
        System.out.println("before->a=" + a + ",b=" + b);
        change(a, b);
        System.out.println("after->a=" + a + ",b=" + b);
        //---------
        Integer x = 1, y = 1;
        System.out.println(x==y);
        x = 130;y = 130;
        System.out.println(x==y);
    }

    /**
     * 交换a与b的值
     * @param a
     * @param b
     * @throws Exception
     */
    public static void change(Integer a, Integer b) throws Exception {
        Field field = a.getClass().getDeclaredField("value");
        field.setAccessible(true);//private final int value;
        Integer tmp = new Integer(a.intValue());
        field.set(a, b);
        field.set(b, tmp);
    }
}
