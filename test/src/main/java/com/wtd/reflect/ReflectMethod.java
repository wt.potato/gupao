package com.wtd.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author wt.d
 * @date 13:58 2018/6/21
 */
public class ReflectMethod {
    public static void main(String[] args) {
        //构造方法获取
        Class<?> clazz = null;
        try {
            clazz = Class.forName("com.wtd.reflect.Student");
            System.out.println("-------------------clazz.getMethods()，获取所有公有方法，包含父类-------------------");
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                System.out.println(method);
            }
            System.out.println("-------------------clazz.getDeclaredMethods()，获取所有成员方法，不包含继承-------------------");
            methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                System.out.println(method);
            }
            System.out.println("-------------------clazz.getDeclaredMethods()，获取公有方法，并调用-------------------");
            Method method = clazz.getMethod("show1", String.class);
            System.out.println(method);
            Object object = clazz.getConstructor().newInstance();
            method.invoke(object, "杨幂");
            System.out.println("-------------------clazz.getDeclaredMethods()，获取所有方法，并调用-------------------");
            method = clazz.getDeclaredMethod("show4", int.class);
            System.out.println(method);
            method.setAccessible(true);//接触私有限定
            Object result = method.invoke(object, 110);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
