package com.wtd.reflect;

import javax.swing.text.html.FormView;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * @author wt.d
 * @date 13:58 2018/6/21
 */
public class ReflectFields {
    public static void main(String[] args) {
        //构造方法获取
        Class<?> clazz = null;
        try {
            clazz = Class.forName("com.wtd.reflect.Student");
            System.out.println("-------------------clazz.getFields()获取公有字段-------------------");
            Field[] fields = clazz.getFields();
            for (Field field : fields) {
                System.out.println(field);
            }

            System.out.println("-------------------clazz.getDeclaredFields()，获取所有字段-------------------");
            fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                System.out.println(field);
            }
            System.out.println("-------------------clazz.getDeclaredFields()，获取公有字段，并调用-------------------");
            Field field = clazz.getField("seqNo");
            System.out.println(field);
            Object object = clazz.getConstructor().newInstance();
            field.set(object, 100001);
            Student student = (Student) object;
            System.out.println("seqNo:" + student.getSeqNo());

            System.out.println("-------------------clazz.getDeclaredFields()，获取所有字段，并调用-------------------");
            field = clazz.getDeclaredField("age");
            System.out.println(field);
            field.setAccessible(true);//暴力反射
            field.set(object, 20);
            student = (Student) object;
            System.out.println("age:" + student.getAge());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
