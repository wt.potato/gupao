package com.wtd.reflect;

/**
 * @author wt.d
 * @date 13:49 2018/6/21
 */
public class Student extends SuperClass {

    private String name;
    private String sex;
    private Integer age;
    public Integer seqNo;
    public static String a = "123";

    public Student() {
        System.out.println("公有，无参构造方法");
    }

    public Student(String name) {
        this.name = name;
    }

    private Student(String name, String sex, Integer age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public Integer getAge() {
        return age;
    }

    public Integer getSeqNo() {
        return seqNo;
    }
    //--------------------------------------
    public void show(){
        System.out.println("is show()");
    }

    public void show1(String s){
        System.out.println("调用了：公有的，String参数的show1(): s = " + s);
    }

    private String show4(int age){
        System.out.println("调用了，私有的，并且有返回值的，int参数的show4(): age = " + age);
        return "大幂幂";
    }

    public static void main(String[] args) {
        System.out.println("main方法执行。。。");
    }
}
