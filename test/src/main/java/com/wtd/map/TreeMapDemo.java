package com.wtd.map;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author wt.d
 * @date 10:58 2018/10/30
 */
public class TreeMapDemo {

    public static void main(String[] args) {
        Map<String, String> map = new TreeMap();
        map.put("1", "java");
        map.put("2", "C");
    }
}
