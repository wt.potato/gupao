package com.wtd.map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wt.d
 * @date 14:27 2018/10/30
 */
public class HashMapDemo {

    public static void main(String[] args) {
//        Map<String, Object> map = new HashMap<>();
//        System.out.println(map.put("1", "语文"));
//        System.out.println(map.put("2", "数学"));
//        map.put("3", "物理");
//        map.put("4", "政治");
//        map.put("5", "英语");
//        map.put("5", "重复");
//
//        map.forEach((k,v)->{
//            System.out.println(k + "->" + v);
//        });

        //打印上一次存储的值
        HashMap<String, String> map = new HashMap<String, String>();
        System.out.println(map.put("a", "A")); // 打印null
        System.out.println(map.put("a", "AA")); // 打印A
        System.out.println(map.put("a", "AB")); // 打印AA
    }
}
