package com.wtd.encryption1;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.Key;
import java.security.SecureRandom;

/**
 * @author wt.d
 * @date 09:26 2018/11/29
 */
public class EncryptUtil {

    private static final String ENCRYPTALGORITHM = "AES/ECB/PKCS5Padding";

    private String key;
    private String str;

    public EncryptUtil(String key, String str) {
        this.key = key;
        this.str = str;
    }

    /**
     * 生成秘钥key
     * @return
     */
    private SecretKey getSecretKey() {
        try {
            KeyGenerator _generator =
                    KeyGenerator.getInstance("AES");
            SecureRandom
                    random=SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(key.getBytes());
            _generator.init(128, random);
            return _generator.generateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 加密 String 明文输入,String 密文输出
     * @return
     */
    public String getEncString() {
        byte[] byteMi = null;
        byte[] byteMing = null;
        String strMi = "";
        BASE64Encoder base64en = new BASE64Encoder();
        try {
            byteMing = str.getBytes("UTF8");
            byteMi = this.getEncCode(byteMing);
            strMi = base64en.encode(byteMi);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            base64en = null;
            byteMing = null;
            byteMi = null;
        }
        return strMi;
    }

    /**
     * 解密 以 String 密文输入,String 明文输出
     * @return
     */
    public String getDesString() {
        BASE64Decoder base64De = new BASE64Decoder();
        byte[] byteMing = null;
        byte[] byteMi = null;
        String strMing = "";
        try {
            byteMi = base64De.decodeBuffer(str);
            byteMing = this.getDesCode(byteMi);
            strMing = new String(byteMing, "UTF8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            base64De = null;
            byteMing = null;
            byteMi = null;
        }
        return strMing;
    }
    /**
     * 为 getEncString 方法提供服务
     * 加密以 byte[]明文输入,byte[]密文输出
     * @param byteS
     * byte[]明文
     * @return byte[]密文
     */
    private byte[] getEncCode(byte[] byteS) {
        byte[] byteFina = null;
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ENCRYPTALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, this.getSecretKey());
            byteFina = cipher.doFinal(byteS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cipher = null;
        }
        return byteFina;
    }

    /**
     * 为 getDesString 方法提供服务
     * 解密以 byte[]密文输入,以 byte[]明文输出
     * @param byteD
     * byte[]密文
     * @return byte[]明文
     */
    private byte[] getDesCode(byte[] byteD) {
        Cipher cipher;
        byte[] byteFina = null;
        try {
            cipher = Cipher.getInstance(ENCRYPTALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, this.getSecretKey());
            byteFina = cipher.doFinal(byteD);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cipher = null;
        }
        return byteFina;
    }

    public static void main(String[] args) {
//        key：string 'datebao_12345678'
//        明文：string 'test aes encrypt'
//        密文：string '5+Ax0RQR3imUJYlOdCdfNTUH4IOm6IA229MWnlyCrgk='
        String key = "datebao_12345678";
//        String str = "{\"id\":\"8\"}";
//        EncryptUtil encryptUtil = new EncryptUtil(key,str);
//        System.out.println(str = encryptUtil.getEncString());
//        String key = "dtb_gs_test";
//        String str = "cD7uTZf9RyFzHq3B/nYM6Q==";
        EncryptUtil encryptUtil1 = new EncryptUtil(key,"cD7uTZf9RyFzHq3B/nYM6Q==");
        System.out.println(encryptUtil1.getDesString());
    }
}
