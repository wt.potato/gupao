package com.wtd.lambda;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wt.d
 * @date 17:00 2018/11/6
 */
public class ListDemo {



    public void oneList(){
        List<String> list = new ArrayList<>();
        list.add("java");
        list.add("C++");
        list.add("spring");

        list.forEach(str -> {
            //在lambda中，此处return功能类似continue
            if (str.equals("java")) return;
            System.out.println(str);
        });
    }

    public void twoList(){
        List<innerClass> innerList = new ArrayList<>();
        innerList.add(new innerClass("a" ,1));
        innerList.add(new innerClass("b" ,2));
        innerList.add(new innerClass("c" ,3));
        innerList.forEach(obj -> {
            if (obj.name.equals("a")) return;
            System.out.println(obj.name);
        });
    }
    public static void main(String[] args) {
        ListDemo list = new ListDemo();
        list.oneList();
        list.twoList();
    }

    private class innerClass {
        private String name;
        private int age;

        public innerClass(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}
