package com.wtd.jvm.loadclass;

public class SuperClass {

    static {
        System.out.println("父类静态代码块");
    }
    public SuperClass() {
        System.out.println("父类构造方法");
    }

    public void method3(){
        System.out.println("父类方法");
    }

    public static void method4(){
        System.out.println("父类静态方法");
    }
}
