package com.wtd.jvm.loadclass;

public class JvmLoadClass extends SuperClass{

    static {
        System.out.println("静态代码块");
    }

    public JvmLoadClass() {
        System.out.println("构造方法");
    }

    public void method1(){
        System.out.println("方法");
    }

    public static void method2(){
        System.out.println("静态方法");
    }

    class inner{

//        static { 匿名类不能有静态代码块
//        }

        public void method3(){
            System.out.println("匿名类方法");
        }

        /**
         * 匿名类不能有静态方法
         */
//        public static void method4(){
//            System.out.println("");
//        }
    }
}
