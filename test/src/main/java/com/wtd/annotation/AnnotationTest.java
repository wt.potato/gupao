package com.wtd.annotation;

/**
 * @author wt.d
 * @date 10:54 2018/6/21
 */
public class AnnotationTest {


    public static void main(String[] args) {
        Class<?> clazz = User.class;
        System.out.println(clazz.getClass());
        MyComponent component = clazz.getAnnotation(MyComponent.class);
        System.out.println("==" +  component.value());
        System.out.println("==" +  component.annotationType());
    }
}
