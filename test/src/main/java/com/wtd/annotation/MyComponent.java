package com.wtd.annotation;

import java.lang.annotation.*;

/**
 * @author wt.d
 * @date 10:55 2018/6/21
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyComponent {

    String value() default "";
}
