package com.wtd.annotation;

/**
 * @author wt.d
 * @date 11:00 2018/6/21
 */
@MyComponent("/web")
public class User {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
