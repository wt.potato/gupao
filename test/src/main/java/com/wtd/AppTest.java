package com.wtd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wt.d
 * @date 16:47 2018/12/6
 */
public class AppTest {

    public static void main(String[] args) {
//        int i = 5%2==0?5/2:5/2+1;
//        System.out.println(i);
//        System.out.println(5/2);
        List<Map<String, Object>> listMap = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("test","123456");
        listMap.add(map);
        System.out.println(listMap.get(0).get("test"));
    }
}
