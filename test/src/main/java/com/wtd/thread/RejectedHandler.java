package com.wtd.thread;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 任务无法处理时执行
 * @author 伟涛
 *
 */
public class RejectedHandler implements RejectedExecutionHandler {

	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		// TODO Auto-generated method stub
		System.out.println("--------------获取线程异常，单线程处理--------------");
		//执行
		r.run();
	}

}
