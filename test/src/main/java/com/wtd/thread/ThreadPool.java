package com.wtd.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPool {

	private final static int corePoolSize = 4;//线程池的个数
	private final static int maximumPoolSize = 8;//线程池最大个数
	private final static long keepAliveTime = 0;//线程空闲时间，单位TimeUnit.SECONDS 秒
	private static ThreadPoolExecutor threadPool = null;//声明线程池
	//有界队列，线程队列（先进先出）
	private static ArrayBlockingQueue<Runnable> taskQueue = new ArrayBlockingQueue<Runnable>(16);


	/**
	 * 启动线程池
	 * @author WTD
	 * @Date 2018-4-26
	 */
	public static void start(){
		System.out.println(" 线程池开启...");
		//线程池创建
		threadPool = new ThreadPoolExecutor(corePoolSize,
				maximumPoolSize, keepAliveTime, TimeUnit.SECONDS,
				taskQueue, new RejectedHandler());
	}

	/**
	 * 提交任务
	 * @param task
	 * @author WTD
	 * @Date 2018-4-26
	 */
	public static void execute(Runnable task){
		threadPool.execute(task);
	}

	/**
	 * 提交任务
	 * @param task
	 * @author WTD
	 * @Date 2018-4-26
	 */
	public static void submit(Runnable task){
		threadPool.submit(task);
	}

	/**
	 * 关闭线程池
	 * @author WTD
	 * @Date 2018-4-26
	 */
	public static void close(){
		threadPool.shutdown();
		System.out.println("线程池关闭，不在接收新任务...");
	}

}
