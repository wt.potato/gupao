package com.wtd.thread;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

/**
 * @author wt.d
 * @date 10:19 2018/9/26
 */
public class CompletableFutureDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        CompletableFuture<String> completableFuture = new CompletableFuture<>();
//        //完成操作，可以由其他线程去做
//        completableFuture.complete("Hello,World");
//
//        String value = completableFuture.get();
//        System.out.println(value);

        //异步执行，阻塞操作
//        CompletableFuture asyncCompletableFuture = CompletableFuture.runAsync(()->{
//            System.out.println("Hello World!");
//        });
//        asyncCompletableFuture.get();

//        CompletableFuture<String> supplyAsyncCompletableFuture = CompletableFuture.supplyAsync(new Supplier<String>() {
//            @Override
//            public String get() {
//                return "Hello World!";
//            }
//        });
//        CompletableFuture<String> supplyAsyncCompletableFuture = CompletableFuture.supplyAsync(()->{
//            return String.format("[Thread : %s]Hello,World ...\n", Thread.currentThread().getName());
//        });
//        String value = supplyAsyncCompletableFuture.get();
//        System.out.println(value);

        CompletableFuture supplyAsyncCompletableFuture = CompletableFuture.supplyAsync(()->{
            return String.format("[Thread : %s]Hello,World ...", Thread.currentThread().getName());
        }).thenApply(value ->{
            return value + " - 来自于数据库";
        }).thenApply(value ->{
            return value + new Date();
        }).thenRun(() ->{
            System.out.println("操作结束");
        }).exceptionally((Throwable e) ->{
            return null;
        });
//        String value = supplyAsyncCompletableFuture.get();
//        System.out.println(value);
        while (!supplyAsyncCompletableFuture.isDone()){

        }
        System.out.println("Starting...");
    }
}
