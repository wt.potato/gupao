package com.wtd.thread;


public class MyFuture extends CustomFuture{
	
	private String startDate;
	private String endDate;

	public MyFuture() {
	}

	public MyFuture(String startDate, String endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Object handle() throws Exception {
		Thread.sleep(2000);
		System.out.println("数据：" + startDate);
		return startDate;
	}
}
