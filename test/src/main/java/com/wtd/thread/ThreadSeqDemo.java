package com.wtd.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wt.d
 * @date 18:03 2018/9/21
 */
public class ThreadSeqDemo {

    static Thread thread1 = new Thread(()->{
        System.out.println("thread1");
    });
    static Thread thread2 = new Thread(()->{
        System.out.println("thread2");
    });
    static Thread thread3 = new Thread(()->{
        System.out.println("thread3");
    });
    static ExecutorService executor = Executors.newSingleThreadExecutor();
    public static void main(String[] args) throws InterruptedException {
        //如何控制线程顺序执行
//        thread1.start();
//        thread1.join();
//        thread2.start();
//        thread2.join();
//        thread3.start();
        executor.submit(thread1);
        executor.submit(thread2);
        executor.submit(thread3);
        executor.shutdown();
    }
}
