package com.wtd.thread;

import java.util.concurrent.*;

/**
 * @author wt.d
 * @date 10:47 2018/6/26
 */
public class ThreadTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        FutureTask<Object> task = null;

        for (int i = 0; i < 5; i++) {
            task = new FutureTask<Object>(new MyFuture(Integer.toString(i), null));
            executorService.submit(task);
        }

        System.out.println(task.get());

        System.out.println("我执行了");

        executorService.shutdown();
    }
}
