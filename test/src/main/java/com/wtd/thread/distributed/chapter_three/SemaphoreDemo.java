package com.wtd.thread.distributed.chapter_three;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    private Semaphore semaphore = new Semaphore(2);

    public void method1(int i){
        try {
            semaphore.acquire(2);//默认取一个信号，可以取多个
            System.out.println("调用远程通信方法->" + i);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            semaphore.release();
        }
    }
}
