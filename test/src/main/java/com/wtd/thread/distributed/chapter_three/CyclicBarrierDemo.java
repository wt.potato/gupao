package com.wtd.thread.distributed.chapter_three;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author wt.d
 * @date 17:14 2018/8/24
 */
public class CyclicBarrierDemo {

    volatile static int n = 0;
    public static void main(String[] args) {
        final CyclicBarrier cyclicBarrier = new CyclicBarrier(11);
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                try {
                    n++;
                    Thread.sleep(n * 1000);
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println("我在等待->" + n + "->" + System.currentTimeMillis());
            }).start();
        }
        try {
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        System.out.println("等待结束->" + n + "->" + System.currentTimeMillis());

        //重置cyclicBarrier循环使用
        cyclicBarrier.reset();
    }
}
