package com.wtd.thread.distributed.chapter_three;

public interface Calculator {

    int add(int a, int b);
}
