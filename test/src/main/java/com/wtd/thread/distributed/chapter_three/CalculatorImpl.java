package com.wtd.thread.distributed.chapter_three;

public class CalculatorImpl implements Calculator {

    @Override
    public int add(int a, int b) {
        return a + b;
    }
}
