package com.wtd.thread.distributed.chapter_three;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class LogHandlerTWO implements InvocationHandler{

    private Object target;

    public Object getInstance(Object target){
        this.target = target;
        Class<?> clazz = target.getClass();
        return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        this.doBefore();
        Object o = method.invoke(this.target, args);
        this.doAfter();
        return o;
    }

    public void doBefore(){
        System.out.println("do this before");
    }

    public void doAfter(){
        System.out.println("do this after");
    }
}
