package com.wtd.thread.distributed.chapter_three;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

public class ExchangerDemo {

    public void method1(){
        final Exchanger<List<Integer>> exchanger = new Exchanger<>();

        new Thread(()->{
            List<Integer> list = new ArrayList<>(2);
            list.add(1);
            list.add(2);
            try {
                list = exchanger.exchange(list);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread1" + list);
        },"Thread1").start();

        new Thread(()->{
            List<Integer> list = new ArrayList<>(2);
            list.add(4);
            list.add(5);
            try {
                list = exchanger.exchange(list);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread2" + list);
        },"Thread2").start();


    }
}
