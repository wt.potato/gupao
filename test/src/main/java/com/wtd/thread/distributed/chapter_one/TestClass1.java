package com.wtd.thread.distributed.chapter_one;

import java.util.HashMap;

/**
 * @author wt.d
 * @date 09:47 2018/8/22
 */
public class TestClass1 {

    private HashMap<String, Integer> map = new HashMap<>();

    public synchronized  void add(String key){
        Integer value = map.get(key);
        if (value == null) {
            map.put(key, 1);
            System.out.println("H1:" + key + "=" + 1);
        } else {
            map.put(key, value + 1);
            System.out.println("H2:" + key + "=" + (value + 1));
        }
    }

}
