package com.wtd.thread.distributed.chapter_three;

public class CalculatorProxy implements Calculator {

    private Calculator calculator;

    CalculatorProxy(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public int add(int a, int b) {
        System.out.println("执行前->" + a + "," + b);
        int result = calculator.add(a, b);
        System.out.println("执行后->" + result);
        return result;
    }
}
