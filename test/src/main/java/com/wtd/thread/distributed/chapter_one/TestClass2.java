package com.wtd.thread.distributed.chapter_one;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wt.d
 * @date 09:47 2018/8/22
 */
public class TestClass2 {

    private ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();

    public void add(String key){
        Integer value = map.get(key);
        if (value == null) {
            map.put(key, 1);
            System.out.println("H1:" + key + "=" + 1);
        } else {
            map.put(key, value + 1);
            System.out.println("H2:" + key + "=" + (value + 1));
        }
    }

    public void mapSize(){
        System.out.println("ConcurrentHashMap个数：" + map.size());
    }
}
