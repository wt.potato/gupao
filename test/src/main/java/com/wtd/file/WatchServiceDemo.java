package com.wtd.file;

import jdk.nashorn.internal.ir.ForNode;

import java.awt.print.PrinterGraphics;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 16:31 2018/9/26
 */
public class WatchServiceDemo {

    public static void main(String[] args) throws IOException {
//        File userDir = new File("src/main");
//        println("用户目录:" + System.getProperty("user.home"));
//        println("用户工作目录:" + System.getProperty("user.dir"));
//        println("用户工作目录:" + userDir.getAbsolutePath());
        WatchService watchService = FileSystems.getDefault().newWatchService();
        //文件对象
        File userDir = new File(System.getProperty("user.dir") + "/test");
        //路径对象 since 1.7
        Path path = userDir.toPath();
        println("监控路径:" + path);
        //注册监听文件事件
        path.register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY);

        while (true) {
            WatchKey watchKey = watchService.poll();
            if (watchKey != null) {
                List<WatchEvent<?>> events = watchKey.pollEvents();
                for (WatchEvent<?> event : events) {
                    println("文件类型:" + event.kind().name());
                    if (StandardWatchEventKinds.ENTRY_CREATE.name().equals(event.kind().name())) {
                        Path subPath = (Path) event.context();
                        println("新建文件:" + subPath);
                    } else if (StandardWatchEventKinds.ENTRY_DELETE.name().equals(event.kind().name())) {
                        Path subPath = (Path) event.context();
                        println("删除文件:" + subPath);
                    } else if (StandardWatchEventKinds.ENTRY_MODIFY.name().equals(event.kind().name())) {
                        Path subPath = (Path) event.context();
                        println("编辑文件:" + subPath);
                    }
                }
                //重置监听
                watchKey.reset();
            }
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void println(Object object){
        System.out.println(object);
    }
}
