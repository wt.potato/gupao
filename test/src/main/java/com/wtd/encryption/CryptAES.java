package com.wtd.encryption;

import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wt.d
 * @date 16:35 2018/11/15
 */
public class CryptAES {

    private static final String AES_TYPE ="AES/ECB/PKCS5Padding";
    private static final String ALGORITHM = "AES";
    private static final String CHARSET = "UTF-8";

    /**
     * 加密
     * @param keyStr
     * @param plainText
     * @return
     */
    public static String aesEncrypt(String keyStr, String plainText) {
        byte[] encryptByte = null;
        try{
            Key key = generateKey(keyStr);
            Cipher cipher = Cipher.getInstance(AES_TYPE);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encryptByte = cipher.doFinal(plainText.getBytes());
        }catch(Exception e){
            e.printStackTrace();
        }
        return new String(Base64.encodeBase64(encryptByte));
    }

    /**
     * 解密
     * @param keyStr
     * @param encryptData
     * @return
     */
    public static String aesDecrypt(String keyStr, String encryptData) {
        byte[] decryptByte = null;
        try{
            Key key = generateKey(keyStr);
            Cipher cipher = Cipher.getInstance(AES_TYPE);
            cipher.init(Cipher.DECRYPT_MODE, key);
            decryptByte = cipher.doFinal(Base64.decodeBase64(encryptData));
        }catch(Exception e){
            e.printStackTrace();
        }
        return new String(decryptByte).trim();
    }

    /**
     * 密钥
     * @param key
     * @return
     * @throws Exception
     */
    private static Key generateKey(String key){
        SecretKeySpec keySpec = null;
        try{
            keySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
        }catch(Exception e){
            e.printStackTrace();
        }
        return keySpec;
    }

    /**
     * URLEncoder
     * @param str
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String urlEncode(String str) throws UnsupportedEncodingException {
        try {
            return URLEncoder.encode(str, CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw e;
        }
    }

    /**
     * URLDecoder
     * @param str
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String urlDecode(String str) throws UnsupportedEncodingException {
        try {
            return URLDecoder.decode(str, CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw e;
        }
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String keyStr = "test";
        String plainText = "test aes encrypt";
        //aes base64 url
        String encText = aesEncrypt(keyStr, plainText);
//        encText = urlEncode(encText);
        System.out.println(encText);
//        url base64 aes
//        encText = urlDecode(encText);
//        String decString = aesDecrypt(keyStr, encText);
//        System.out.println(decString);

//        cryptStr2 = urlDecode(cryptStr2);
//        String decString = aesDecrypt(keyStr, cryptStr2);
//        //解析后中文变为Unicode编码，是否需要再次解码？
//        System.out.println(decString);
//        System.out.println(unicodeToStr("\\u8ba2\\u5355\\u72b6\\u6001"));
    }

    /**
     * 字符串转Unicode
     * @param str
     * @return
     */
    public static String strToUnicode(String str) {
        StringBuffer unicode = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            unicode.append("\\u" +Integer.toHexString(c));
        }
        return unicode.toString();
    }

    /**
     * Unicode转字符串
     * @param unicode
     * @return
     */
    public static String unicodeToStr(String unicode) {
        StringBuffer buffer = new StringBuffer();
        String[] hex = unicode.split("\\\\u");
        for (int i = 1; i < hex.length; i++) {
            int data = Integer.parseInt(hex[i], 16);
            buffer.append((char) data);
        }
        return buffer.toString();
    }
}
