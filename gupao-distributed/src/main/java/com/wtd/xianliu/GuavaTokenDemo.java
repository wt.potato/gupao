package com.wtd.xianliu;

import com.google.common.util.concurrent.RateLimiter;

import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 15:44 2018/10/24
 */
public class GuavaTokenDemo {

    private int qps;
    private int countOfReq;
    private RateLimiter rateLimiter;

    public GuavaTokenDemo(int qps, int countOfReq) {
        this.qps = qps;
        this.countOfReq = countOfReq;
    }

    /**
     * 初始化一个令牌桶
     * @return
     */
    public GuavaTokenDemo processWithTokenBucket(){
        rateLimiter = RateLimiter.create(qps);
        System.out.println("初始化令牌桶:" + rateLimiter.getClass());
        return this;
    }

    /**
     * 创建一个漏桶
     * @return
     */
    public GuavaTokenDemo processWithleakyBucket(){
        rateLimiter=RateLimiter.create(qps,0,TimeUnit.MILLISECONDS);
        System.out.println("初始化漏桶:" + rateLimiter.getClass());
        return this;
    }

    /**
     * 模拟请求
     */
    private void processRequest(){
        System.out.println("RateLimiter:" + rateLimiter.getClass());
        long start = System.currentTimeMillis();
        for (int i = 0; i < countOfReq; i++) {
            rateLimiter.acquire();
        }
        long end = System.currentTimeMillis() - start;
        System.out.println("处理请求的数量:" + countOfReq + ",耗时:" + end + ",qps:"
                + rateLimiter.getRate() + ",实际qps:" + Math.ceil(countOfReq/(end/1000.00)));
    }

    /**
     * 调用请求
     * @throws InterruptedException
     */
    public void doProcess() throws InterruptedException {
        for (int i = 0; i < 20; i = i + 10) {
            TimeUnit.SECONDS.sleep(i);
            processRequest();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new GuavaTokenDemo(50,100).processWithTokenBucket().doProcess();
        new GuavaTokenDemo(50,100).processWithleakyBucket().doProcess();
    }
}
