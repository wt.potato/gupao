package com.wtd.rmi.rpc;

public class GpHelloImpl implements IGpHello {

    @Override
    public String sayhello(String msg) {
        return "Hello," + msg;
    }
}
