package com.wtd.rmi.rpc.client;

import java.lang.reflect.Proxy;

public class RpcClientProxy {

    public <T> T clientProxy(final Class<T> interfaceCLS,
                             final String host, final int port){
        return (T) Proxy.newProxyInstance(interfaceCLS.getClassLoader(),
                new Class[]{interfaceCLS}, new RemoteInvocationHandler(host, port));
    }
}
