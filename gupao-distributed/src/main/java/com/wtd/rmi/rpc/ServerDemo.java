package com.wtd.rmi.rpc;

public class ServerDemo {

    public static void main(String[] args) {
        IGpHello gpHello = new GpHelloImpl();
        RpcServer server = new RpcServer();
        server.pushlisher(gpHello, 8888);
    }
}
