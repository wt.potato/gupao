package com.wtd.rmi.rpc.client;

import com.wtd.rmi.rpc.IGpHello;

public class RpcClientDemo {

    public static void main(String[] args) {
        RpcClientProxy rpcClientProxy = new RpcClientProxy();

        IGpHello gpHello = rpcClientProxy.clientProxy(IGpHello.class, "localhost", 8888);
        System.out.println(gpHello.sayhello("Mic"));
    }
}
