package com.wtd.rmi.client;

import com.wtd.rmi.IHelloService;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ClientDemo {

    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        //客户端接口架包目录要与服务端架包目录保持一致，否则类无法转换
        IHelloService helloService = (IHelloService) Naming.lookup("rmi://127.0.0.1/Hello");
        System.out.println(helloService.sayHello("wt.d"));

    }
}
