package com.wtd.serial;

import com.alibaba.fastjson.JSON;

public class FastJsonSerializer implements ISerializer {

    @Override
    public <T> byte[] serialier(T obj) {
        return JSON.toJSONString(obj).getBytes();
    }


    @Override
    public <T> T deSerialier(byte[] data, Class<T> clazz) {
        return JSON.parseObject(new String(data), clazz);
    }
}
