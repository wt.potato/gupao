package com.wtd.serial.bean;

import java.io.Serializable;

/**
 * @author wt.d
 * @date 16:38 2018/6/8
 */
public class SuperUser implements Serializable{

    String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
