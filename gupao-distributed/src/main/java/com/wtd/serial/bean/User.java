package com.wtd.serial.bean;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author wt.d
 * @date 16:20 2018/6/8
 */
public class User extends SuperUser {

    private static final long serialVersionUID = -9127690313096607951L;

    //序列化后新增，反序列化测试：类型初始值null
    private String newName;

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    private String name;
    private int age;

    //验证静态变量能否被序列化：序列化保存的事对象的状态，静态变量属于类的状态，因此序列化不保存静态变量
    public static int num = 5;

    //transient关键字控制变量的序列化，可以阻止变量序列化到文件中，饭序列化后该变量值被设为初始值，如 int 0，对象 null
    private transient String hobby;

    /**
     * 序列化，避开关键字transient
     * 只针对java有效
     * @param objectOutputStream
     * @throws IOException
     */
    private void  writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(hobby);
        System.out.println("我执行了序列化");
    }

    /**
     * 反序列化，避开关键字transient
     * 只针对java有效
     * @param objectInputStream
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void  readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        hobby = (String) objectInputStream.readObject();
        System.out.println("我执行了反序列化");
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
