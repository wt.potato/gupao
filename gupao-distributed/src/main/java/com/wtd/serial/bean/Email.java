package com.wtd.serial.bean;

import java.io.Serializable;

/**
 * @author wt.d
 * @date 18:33 2018/6/8
 */
public class Email implements Serializable{

    private static final long serialVersionUID = 1801579581306236813L;
    private String context;

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
