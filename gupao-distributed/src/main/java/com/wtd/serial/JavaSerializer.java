package com.wtd.serial;

import java.io.*;

/**
 * @author wt.d
 * @date 16:10 2018/6/8
 */
public class JavaSerializer implements  ISerializer {

    @Override
    public <T> byte[] serialier(T obj) {
        ByteArrayOutputStream byteArrayOutputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {

            byteArrayOutputStream = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try {
                objectOutputStream.close();
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new byte[0];
    }

    @Override
    public <T> T deSerialier(byte[] data, Class<T> clazz) {
        ByteArrayInputStream  byteArrayInputStream = null;
        ObjectInput objectInput = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(data);
            objectInput = new ObjectInputStream(byteArrayInputStream);
            return (T) objectInput.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                objectInput.close();
                byteArrayInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return null;
    }
}
