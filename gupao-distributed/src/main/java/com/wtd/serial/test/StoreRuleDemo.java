package com.wtd.serial.test;

import com.wtd.serial.bean.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author wt.d
 * @date 16:59 2018/6/8
 */
public class StoreRuleDemo {

    public static void main(String[] args) throws IOException {
//        //序列化
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("user.txt"));
//        User user = new User();
//        user.setName("Mic");
//
//        objectOutputStream.writeObject(user);
//        objectOutputStream.flush();
//        System.out.println(new File("user.txt").length());
//
//        objectOutputStream.writeObject(user);
//        objectOutputStream.close();
//        System.out.println(new File("user.txt").length());

        try {
            //反序列化
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("user.txt"));
            User deUser = (User) objectInputStream.readObject();
            System.out.println(deUser.getNewName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //反序列化时增加减少字段不影响
        //反序列化时如果serialVersionUID不一致报如下错误
//        Exception in thread "main" java.io.InvalidClassException: com.wtd.serial.bean.User; local class incompatible: stream classdesc serialVersionUID = -9127690313096607951, local class serialVersionUID = 1
//        at java.io.ObjectStreamClass.initNonProxy(ObjectStreamClass.java:687)
//        at java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1883)
//        at java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1749)
//        at java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2040)
//        at java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1571)
//        at java.io.ObjectInputStream.readObject(ObjectInputStream.java:431)
//        at com.wtd.serial.test.StoreRuleDemo.main(StoreRuleDemo.java:30)
    }
}
