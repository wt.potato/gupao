package com.wtd.serial.test;

import com.wtd.serial.bean.Email;
import com.wtd.serial.bean.Person;

import java.io.IOException;

/**
 * @author wt.d
 * @date 18:43 2018/6/8
 */
public class CloneDemo {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Email email = new Email();
        email.setContext("明天晚上八点有课");

        Person person = new Person();
        person.setName("开发A");
        person.setEmail(email);

        try {

//            System.out.println("--------------------浅克隆--------------------------------------------");
//            Person clonePerson = (Person) person.clone();
//            clonePerson.setName("开发B");
//            clonePerson.getEmail().setContext("周三八点半有课");
//            System.out.println(person.getName() + "," + person.getEmail().getContext());
//            System.out.println(clonePerson.getName() + "," + clonePerson.getEmail().getContext());

            System.out.println("---------------------深克隆-------------------------------------------");
            Person deepPerson = (Person) person.deepClone();
            deepPerson.setName("开发B");
            deepPerson.getEmail().setContext("周三八点半有课");
            System.out.println(person.getName() + "," + person.getEmail().getContext());
            System.out.println(deepPerson.getName() + "," + deepPerson.getEmail().getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
