package com.wtd.serial.test;

import com.wtd.serial.ISerializer;
import com.wtd.serial.JavaSerializer;
import com.wtd.serial.bean.User;

/**
 * @author wt.d
 * @date 16:23 2018/6/8
 */
public class Demo {

    public static void main(String[] args) {
        ISerializer serializer = new JavaSerializer();
//        ISerializer serializer = new FastJsonSerializer();
        User user = new User();
        user.setName("Mic");
        user.setAge(18);
        user.setSex("男");
        user.setHobby("菲菲");

        //序列化
        byte[] bytes = serializer.serialier(user);
        user.num = 10;

        //反序列化
        User deUser = serializer.deSerialier(bytes, User.class);

        System.out.println(deUser.getName() + "--" + deUser.getAge());
        //静态变量序列化测试：序列化保存的事对象的状态，静态变量属于类的状态，因此序列化不保存静态变量
        System.out.println(deUser.num);

        //子类实现序列化，父类不实现序列化测试：序列化—反序列化后无法获取该变量属性值（属性值为null）
        System.out.println(deUser.getSex());

        //transient关键字控制变量的序列化，可以阻止变量序列化到文件中，饭序列化后该变量值被设为初始值，如 int 0，对象 null
        //使用fastjson进行序列化、反序列化没效果
        System.out.println(deUser.getHobby());
    }
}
