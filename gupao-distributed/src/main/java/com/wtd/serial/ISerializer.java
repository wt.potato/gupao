package com.wtd.serial;

/**
 * @author wt.d
 * @date 16:05 2018/6/8
 */
public interface ISerializer {

    /**
     * 序列化
     * @param obj
     * @param <T>
     * @return
     */
    <T> byte[] serialier(T obj);

    /**
     * 反序列化
     * @param data
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T deSerialier(byte[] data, Class<T> clazz);
}
