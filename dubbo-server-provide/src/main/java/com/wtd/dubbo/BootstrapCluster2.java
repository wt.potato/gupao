package com.wtd.dubbo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author wt.d
 * @date 15:45 2018/6/14
 */
public class BootstrapCluster2 {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/spring/dubbo-cluster2.xml");
        context.start();
        System.in.read();
    }
}
