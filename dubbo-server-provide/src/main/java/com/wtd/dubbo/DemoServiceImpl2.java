package com.wtd.dubbo;

/**
 * @author wt.d
 * @date 17:49 2018/6/15
 */
public class DemoServiceImpl2 implements IDemoService {

    @Override
    public String protocolDemo(String msg) {
        return "I'm Protocol v1.0.1 Demo:"+msg;
    }
}
