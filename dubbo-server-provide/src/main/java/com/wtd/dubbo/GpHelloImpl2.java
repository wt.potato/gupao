package com.wtd.dubbo;

/**
 * @author wt.d
 * @date 15:13 2018/6/14
 */
public class GpHelloImpl2 implements IGpHello {

    @Override
    public String sayHello(String msg) {
        return "I'm cluster2 Hello," + msg;
    }
}
