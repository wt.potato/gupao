package com.wtd.spring.formework.beans;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

/**
 * 用来存储配置文件中的信息
 * 相当于保存在内存中的配置
 */
public class GPBeanDefintion {

    private String beanClassname;
    private Boolean lazyInit = false;
    private String factoryName;

    public String getBeanClassname() {
        return beanClassname;
    }

    public void setBeanClassname(String beanClassname) {
        this.beanClassname = beanClassname;
    }

    public Boolean isLazyInit() {
        return lazyInit;
    }

    public void setLazyInit(Boolean lazyInit) {
        this.lazyInit = lazyInit;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }
}
