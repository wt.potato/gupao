package com.wtd.spring.formework.annotation;

import java.lang.annotation.*;

/**
 * 业务逻辑，注解接口
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GPService {

    String value() default "";
}
