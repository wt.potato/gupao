package com.wtd.spring.formework.context;

import com.wtd.spring.formework.annotation.GPAutoWired;
import com.wtd.spring.formework.annotation.GPController;
import com.wtd.spring.formework.annotation.GPService;
import com.wtd.spring.formework.aop.GPAopConfig;
import com.wtd.spring.formework.beans.GPBeanDefintion;
import com.wtd.spring.formework.beans.GPBeanPostProcessor;
import com.wtd.spring.formework.beans.GPBeanWrapper;
import com.wtd.spring.formework.context.support.GPBeanDefintionReader;
import com.wtd.spring.formework.core.GPBeanFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GPApplicationContext extends GPDefaultListableBeanFactory implements GPBeanFactory{

    private String[] configLocations;

    private GPBeanDefintionReader reader;

    //用来保存注册时单例的容器
    private Map<String, Object> beanCachemap = new HashMap<>();

    //用来存储所有的被代理过的对象
    private Map<String, GPBeanWrapper> beanWrapperMap = new ConcurrentHashMap<>();

    public GPApplicationContext(String... configLocations) {
        this.configLocations = configLocations;

        refresh();
    }

    public void refresh(){
        //定位
        this.reader = new GPBeanDefintionReader(configLocations);

        //加载
        List<String> beanDefintions = reader.loadBeanDefintions();

        //注册
        doRegisty(beanDefintions);

        //依赖注入（lazy-init = false）,要是执行依赖注入
        //在这里自动调用getBean方法
        doAutowrited();

    }

    /**
     * 开始执行自动化的依赖注入
     */
    private void doAutowrited(){
        for (Map.Entry<String, GPBeanDefintion> beanDefintionEntry : this.beanDefinitionMap.entrySet()){
            String beanName = beanDefintionEntry.getKey();

            if (!beanDefintionEntry.getValue().isLazyInit()){
                Object obj = getBean(beanName);
            }
        }

        for (Map.Entry<String, GPBeanWrapper> beanWrapperEntry : this.beanWrapperMap.entrySet()){
            populateBean(beanWrapperEntry.getKey(), beanWrapperEntry.getValue().getOriginalInstance());
        }

    }

    public void populateBean(String beanName, Object instance){
        Class clazz = instance.getClass();

        //不是所有牛奶都叫特仑苏
        if (!(clazz.isAnnotationPresent(GPController.class)
                || clazz.isAnnotationPresent(GPService.class))){
            return;
        }

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields){
            if(!field.isAnnotationPresent(GPAutoWired.class)){
                continue;
            }

            GPAutoWired autoWired = field.getAnnotation(GPAutoWired.class);

            String autowireBeanName = autoWired.value().trim();

            if ("".equals(autowireBeanName)){
                autowireBeanName = field.getType().getName();
            }

            field.setAccessible(true);

            try {
                field.set(instance, this.beanWrapperMap.get(autowireBeanName).getWrapperInstance());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    /**
     * 依赖注入，从这里开始，通过获取BeanDefinition中的信息
     * 然后，通过反射机制创建一个实例并返回
     * spring做法是，不会把最原始的对象放出去，会用一个BeanWrapper来进行一次包装
     * 装饰器模式：
     * 1.保留原来的oop关系
     * 2.我需要对它进行扩展，增强（为了已有AOP打基础）
     * @param beanName
     * @return
     */
    @Override
    public Object getBean(String beanName){
        GPBeanDefintion beanDefintion = this.beanDefinitionMap.get(beanName);

        String className = beanDefintion.getBeanClassname();
        try {
            //生成通知事件
            GPBeanPostProcessor beanPostProcessor = new GPBeanPostProcessor();

            Object instance = instantionBean(beanDefintion);
            if (instance == null) {
                return null;
            }

            //在实例初始化以前调用一次
            beanPostProcessor.postProcessBeforeInitialization(instance, beanName);

            GPBeanWrapper beanWrapper = new GPBeanWrapper(instance);
            beanWrapper.setAopConfig(instantionAopConfig(beanDefintion));
            beanWrapper.setPostProcessor(beanPostProcessor);
            this.beanWrapperMap.put(beanName, beanWrapper);
            //在实例初始化以后调用一次
            beanPostProcessor.postProcessAfterInitialization(instance, beanName);

            //通过这样一调用，相当于给我们自己留有了可操作的空间
            return this.beanWrapperMap.get(beanName).getWrapperInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public GPAopConfig instantionAopConfig(GPBeanDefintion beanDefintion) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException {
            GPAopConfig config = new GPAopConfig();
            String expression = reader.getConfig().getProperty("pointCut");
            String[] before = reader.getConfig().getProperty("aspectBefore").split("\\s");
            String[] after = reader.getConfig().getProperty("aspectAfter").split("\\s");

            String className = beanDefintion.getBeanClassname();
            Class<?> clazz = Class.forName(className);

        Pattern pattern = Pattern.compile(expression);

        Class aspectClass = Class.forName(before[0]);

        //在这里得到的方法都是原生的方法
        for (Method m : clazz.getMethods()){
            Matcher matcher = pattern.matcher(m.toString());
            if (matcher.matches()){
                config.put(m, aspectClass.newInstance(), new Method[]{aspectClass.getMethod(before[1]), aspectClass.getMethod(after[1])});
            }
        }

        return config;
    }

    /**
     * 传一个BeanDefintion，就返回一个实例Bean
     * @param beanDefintion
     * @return
     */
    private Object instantionBean(GPBeanDefintion beanDefintion){
        Object instance = null;
        String className = beanDefintion.getBeanClassname();
        try {
            if (this.beanCachemap.containsKey(className)){
                instance = this.beanCachemap.get(className);
            } else {
                Class<?> clazz = Class.forName(className);
                instance = clazz.newInstance();
                this.beanCachemap.put(className, instance);
            }

            return instance;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void doRegisty(List<String> beanDefintions){
        //beanName有三种情况：
        //1. 默认是类名首字母小写
        //2. 自定义名字
        //3. 接口注入

        try {
            for (String className : beanDefintions){
                Class<?> beanClass = Class.forName(className);

                //如果是一个接口，是不能实例化的
                //用它的实现类来实例化
                if (beanClass.isInterface()) {
                    continue;
                }

                GPBeanDefintion beanDefintion = reader.registerBean(className);

                if(beanDefintion != null) {
                    this.beanDefinitionMap.put(beanDefintion.getFactoryName(), beanDefintion);
                }

                Class<?>[]  interfaces = beanClass.getInterfaces();
                for (Class<?> i : interfaces){
                    //如果是多个实现类，spring只能覆盖
                    //为什么，因为spring没有那么智能，就是那么的傻
                    //这个时候可以自定义名字
                    this.beanDefinitionMap.put(i.getName(), beanDefintion);
                }

                //到这里为止，容器初始化完毕
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public String[] getBeanDefinitionNames() {
        return this.beanDefinitionMap.keySet().toArray(new String[this.beanDefinitionMap.size()]);
    }


    public int getBeanDefinitionCount() {
        return  this.beanDefinitionMap.size();
    }


    public Properties getConfig(){
        return this.reader.getConfig();
    }


}
