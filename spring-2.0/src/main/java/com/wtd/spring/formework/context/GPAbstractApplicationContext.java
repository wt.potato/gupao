package com.wtd.spring.formework.context;

public abstract class GPAbstractApplicationContext {

    /**
     * 提供给子类重写
     */
    protected void onReFresh(){

    }

    protected abstract void refreshBeanFactory();
}
