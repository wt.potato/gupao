package com.wtd.spring.formework.context.support;

import com.wtd.spring.formework.beans.GPBeanDefintion;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 对配置文件进行查找，读取，解析
 */
public class GPBeanDefintionReader {

    private Properties config = new Properties();

    private List<String> registyBeanClasses = new ArrayList<>();

    //在配置文件中，用来获取自动扫描的包名的key
    private final String SCAN_PPACKAGE = "scanPackage";

    public GPBeanDefintionReader(String ... locations) {
        //在spring中是通过Reader去查找和定位的
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(locations[0].replace("classpath:", ""));

        try {
            config.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        doScanner(config.getProperty(SCAN_PPACKAGE));
    }

    public List<String> loadBeanDefintions() {
        return  this.registyBeanClasses;
    }

    /**
     * 每注册一个className就返回一个BeanDefintion,我自己包装
     * 只是为了对配置信息进行一个包装
     * @param className
     * @return
     */
    public GPBeanDefintion registerBean(String className){
        if (this.registyBeanClasses.contains(className)) {
            GPBeanDefintion beanDefintion = new GPBeanDefintion();
            beanDefintion.setBeanClassname(className);
            beanDefintion.setFactoryName(lowerFirstCase(className.substring(className.lastIndexOf(".") + 1)));
            return beanDefintion;
        }
        return null;
    }

    /**
     * 递归扫描所有的相关联的class，并保存到一个List中
     * @param packageName
     */
    private void doScanner(String packageName){
        URL url = this.getClass().getClassLoader().getResource("/" + packageName.replaceAll("\\.", "/"));

        File classDir = new File(url.getFile());

        for (File file : classDir.listFiles()){
            if (file.isDirectory()){
                doScanner(packageName + "." + file.getName());
            } else {
                registyBeanClasses.add(packageName + "." + file.getName().replace(".class", ""));
            }
        }
    }

    public Properties getConfig() {
        return this.config;
    }

    private String lowerFirstCase(String str){
        char [] chars = str.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }
}
