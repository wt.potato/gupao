package com.wtd.mybatis.demo;

import com.wtd.mybatis.beans.Test;
import com.wtd.mybatis.mapper.TestMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Demo {

    public static SqlSession getSqlSession() throws FileNotFoundException {
        InputStream configFile = new FileInputStream("C:\\Users\\Administrator\\IdeaProjects\\gupao\\mybatis\\src\\main\\java\\com\\wtd\\mybatis\\demo\\mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configFile);
        return sqlSessionFactory.openSession(true);
    }

    public static void main(String[] args) throws FileNotFoundException {
        TestMapper testMapper = getSqlSession().getMapper(TestMapper.class);
        Test test = new Test(null,null,"李四");
        testMapper.insert(test);
        test = testMapper.selectByPrimaryKey(4);
    }
}
