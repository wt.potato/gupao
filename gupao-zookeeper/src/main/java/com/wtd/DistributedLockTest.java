package com.wtd;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @author wt.d
 * @date 18:04 2018/6/12
 */
public class DistributedLockTest {

    public static void main(String[] args) throws IOException {
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                try {
                    countDownLatch.await();
                    DistributedLock distributedLock = new DistributedLock();
                    distributedLock.lock();
                    //等待10s中释放锁
                    Thread.sleep(1000 * 10);
                    distributedLock.unlock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },"Thread" + i).start();
            countDownLatch.countDown();
        }

        System.in.read();
    }
}
