package com.wtd;

import com.wtd.conf.Conf;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class ConnectionDemo {

    public static void main(String[] args) {
        ZooKeeper zooKeeper = null;
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            zooKeeper = new ZooKeeper(Conf.connectString, Conf.sessionTimeout, new Watcher() {
                @Override
                public void process(WatchedEvent watchedEvent) {
                    if (Event.KeeperState.SyncConnected == watchedEvent.getState()){
                        countDownLatch.countDown();
                    }
                }
            });
//            Thread.sleep(1000);
            countDownLatch.await();
            System.out.println("zookeeper启动状态：" + zooKeeper.getState());
            //---------------------------------------------------------------
            //添加节点
            zooKeeper.create("/zk-persis-mic", "0".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            Thread.sleep(1000);
            Stat stat = new Stat();

            //得到当前节点的值
            byte[] bytes=zooKeeper.getData("/zk-persis-mic",null,stat);
            System.out.println("当前节点之：" + new String(bytes));

            //修改节点值
            zooKeeper.setData("/zk-persis-mic", "1".getBytes(), stat.getVersion());

            //得到当前节点的值
            byte[] bytes1=zooKeeper.getData("/zk-persis-mic",null,stat);
            System.out.println("当前节点之：" + new String(bytes1));

            zooKeeper.close();
            System.out.println("阻塞当前线程：" + System.in.read());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }
}
