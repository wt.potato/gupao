package com.wtd.rpc;


import com.wtd.rpc.anno.RpcAnnotation;

@RpcAnnotation(IGpHello.class)
public class GpHelloImpl implements IGpHello {

    @Override
    public String sayhello(String msg) {
        return "I'm 8080 Node," + msg;
    }
}
