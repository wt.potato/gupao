package com.wtd.rpc;



import com.wtd.rpc.zk.IRegisterCenter;
import com.wtd.rpc.zk.RegisterCenterImpl;

import java.io.IOException;

public class LBServerDemo2 {

    public static void main(String[] args) throws IOException {
        IGpHello gpHello = new GpHelloImpl2();
        IRegisterCenter registerCenter = new RegisterCenterImpl();

        RpcServer server = new RpcServer(registerCenter, "127.0.0.1:8081");
        server.bind(gpHello);
        server.pushlisher();
        System.in.read();
    }
}
