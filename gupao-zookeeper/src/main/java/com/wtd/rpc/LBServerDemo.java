package com.wtd.rpc;



import com.wtd.rpc.zk.IRegisterCenter;
import com.wtd.rpc.zk.RegisterCenterImpl;

import java.io.IOException;

public class LBServerDemo {

    public static void main(String[] args) throws IOException {
        IGpHello gpHello = new GpHelloImpl();
        IRegisterCenter registerCenter = new RegisterCenterImpl();

        RpcServer server = new RpcServer(registerCenter, "127.0.0.1:8080");
        server.bind(gpHello);
        server.pushlisher();
        System.in.read();
    }
}
