package com.wtd.rpc;


import com.wtd.rpc.anno.RpcAnnotation;

//@RpcAnnotation(value = IGpHello.class)
@RpcAnnotation(value = IGpHello.class, version = "2.0") //版本区分
public class GpHelloImpl2 implements IGpHello {

    @Override
    public String sayhello(String msg) {
        return "I'm 8081 Node," + msg;
    }
}
