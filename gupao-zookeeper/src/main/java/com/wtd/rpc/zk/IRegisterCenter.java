package com.wtd.rpc.zk;

public interface IRegisterCenter {

    /**
     * 注册服务名称，地址
     * @param serviceName
     * @param serviceAddress
     */
    void register(String serviceName, String serviceAddress);
}
