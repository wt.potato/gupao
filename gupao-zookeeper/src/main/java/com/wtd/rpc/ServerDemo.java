package com.wtd.rpc;

import com.wtd.rpc.zk.IRegisterCenter;
import com.wtd.rpc.zk.RegisterCenterImpl;

import java.io.IOException;

public class ServerDemo {

    public static void main(String[] args) throws IOException {
        IGpHello gpHello = new GpHelloImpl();
        IGpHello gpHello2 = new GpHelloImpl2();

        IRegisterCenter registerCenter = new RegisterCenterImpl();

        RpcServer server = new RpcServer(registerCenter, "127.0.0.1:8080");
        server.bind(gpHello, gpHello2);
        server.pushlisher();
        System.in.read();
    }
}
