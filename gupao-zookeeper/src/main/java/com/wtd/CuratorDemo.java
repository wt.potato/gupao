package com.wtd;

import com.wtd.conf.Conf;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;

/**
 * @author wt.d
 * @date 13:51 2018/6/12
 */
public class CuratorDemo {

    public static void main(String[] args) {
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(Conf.connectString)
                .sessionTimeoutMs(Conf.sessionTimeout)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .namespace("curator")
                .build();
        //启动
        curatorFramework.start();
        System.out.println("启动状态：" + curatorFramework.getState());

        try {
            //创建节点，结果/curator/mic/node-1
            //注意：原生API中，必须是逐层创建，也就是父节点必须存在子节点才能创建
            curatorFramework.create()
                    .creatingParentsIfNeeded()
                    .withMode(CreateMode.PERSISTENT)
                    .forPath("/mic/node-1", "1".getBytes());
            //删除节点
//            curatorFramework.delete().deletingChildrenIfNeeded().forPath("/mic/node-1");

            //设置节点
//            Stat stat = new Stat();
//            curatorFramework.getData()
//                    .storingStatIn(stat)
//                    .forPath("/mic/node1");
//            System.out.println("节点信息:" + stat.toString());
//
//            curatorFramework.setData()
//                    .withVersion(stat.getVersion())
//                    .forPath("/mic/node1", "xx".getBytes());
//            curatorFramework.getData()
//                    .storingStatIn(stat)
//                    .forPath("/mic/node1");
//            System.out.println("节点信息:" + stat.toString());

            curatorFramework.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
