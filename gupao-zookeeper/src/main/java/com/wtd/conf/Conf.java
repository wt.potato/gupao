package com.wtd.conf;

/**
 * @author wt.d
 * @date 11:12 2018/6/12
 */
public class Conf {

//    public final static String connectString = "192.168.80.132:2181," +
//            "192.168.80.133:2181," +
//            "192.168.80.134:2181";

    public final static String connectString = "192.168.139.129:2181," +
            "192.168.139.130:2181," +
            "192.168.139.131:2181";

    public final static int sessionTimeout = 4000;

    public final static String ZK_REGISTER_PATH = "/register";
}
