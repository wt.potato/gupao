package com.wtd;

import com.wtd.conf.Conf;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @author wt.d
 * @date 11:19 2018/6/12
 */
public class WatcherDemo {

    public static void main(String[] args) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        try {
            final ZooKeeper zooKeeper = new ZooKeeper(Conf.connectString, Conf.sessionTimeout, new Watcher() {
                @Override
                public void process(WatchedEvent watchedEvent) {
                    System.out.println("默认的监听：" + watchedEvent.getType() + "->" + watchedEvent.getPath());
                    if (Event.KeeperState.SyncConnected == watchedEvent.getState()){
                        countDownLatch.countDown();
                    }
                }
            });
            countDownLatch.await();
            System.out.println("zookeeper启动状态：" + zooKeeper.getState());
            //-------------------------------------------
            //CreateMode.PERSISTENT 持续节点
            //CreateMode.PERSISTENT_SEQUENTIAL 持续有序节点
            //CreateMode.EPHEMERAL 临时节点
            //CreateMode.EPHEMERAL_SEQUENTIAL 临时有序节点
            //创建节点zk-persis-mic
            zooKeeper.create("/zk-persis-mic", "1".getBytes(),
                    ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

            Stat stat = zooKeeper.exists("/zk-persis-mic", new Watcher() {
                @Override
                public void process(WatchedEvent watchedEvent) {
                    System.out.println(watchedEvent.getType() + "->" + watchedEvent.getPath());

                    //再次注册监听事件
                    try {
                        zooKeeper.exists(watchedEvent.getPath(), true);
                    } catch (KeeperException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            //通过修改的事务类型操作来触发监听事件
            stat = zooKeeper.setData("/zk-persis-mic", "2".getBytes(), stat.getVersion());

            Thread.sleep(1000);

            zooKeeper.delete("/zk-persis-mic", stat.getVersion());

            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }
}
