package com.wtd.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class JMSQueueProducer2 {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("tcp://192.168.80.136:61616");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();

            Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建发送者
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);//消息持久化存储,宕机恢复（持久化订阅）。视情况而定
            //创建需要发送的消息
            TextMessage message = null;
            for (int i = 0; i < 10; i++) {
                message = session.createTextMessage("Hello World " + i);
                producer.send(message);
            }
            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
