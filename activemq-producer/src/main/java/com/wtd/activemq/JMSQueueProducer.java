package com.wtd.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class JMSQueueProducer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("tcp://192.168.80.135:61616");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            /*
            * 事务型会话  Boolean.TRUE
            * Session.AUTO_ACKNOWLEDGE 自动确认
            * 客户端无须手动签收，但必须保证发送端与接收端都是事务型会话
            * 非事务型会话 Boolean.FALSE
            * Session.AUTO_ACKNOWLEDGE 自动确认
            * Session.CLIENT_ACKNOWLEDGE 客户端确认
            * Session.DUPS_OK_ACKNOWLEDGE 延迟确认，指定消息提供者在消息接收者没有确认发送时重新发送消息，这种
            * 模式不在乎接受者收到重复的消息（如：消息处理完成未确认，mq重启（重发由broker操作））
             */
            //Session.SESSION_TRANSACTED  ？？？
            Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建发送者
            MessageProducer producer = session.createProducer(destination);
//            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);//消息持久化存储,宕机恢复（持久化订阅）。视情况而定
            //创建需要发送的消息
            TextMessage message = null;
            for (int i = 0; i < 1000; i++) {
                message = session.createTextMessage("Hello World " + i);
//                message.setStringProperty("wt.d", "hello wt.d " + i);
                producer.send(message);
            }
//            session.commit();   //非事务型会话无须提交
            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
