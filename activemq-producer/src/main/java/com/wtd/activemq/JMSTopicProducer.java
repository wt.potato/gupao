package com.wtd.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class JMSTopicProducer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("tcp://192.168.139.133:61616");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();

            //Session.AUTO_ACKNOWLEDGE 自动确认
            //Session.CLIENT_ACKNOWLEDGE 客户端确认
            //Session.DUPS_OK_ACKNOWLEDGE 延迟确认，指定消息提供者在消息接收者没有确认发送时重新发送消息，这种模式不在乎接受者收到重复的消息
            //Session.SESSION_TRANSACTED
            Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createTopic("myTopic");
            //创建发送者
            MessageProducer producer = session.createProducer(destination);
//            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);//消息非持久化存储
//            producer.setDeliveryMode(DeliveryMode.PERSISTENT);//消息持久化存储
            //创建需要发送的消息
            TextMessage message = message = session.createTextMessage("vip上课时间，周三、周六、周日");
            message.setStringProperty("上课时间", "周三、周六、周日");
            producer.send(message);
            session.commit();
            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
