package com.wtd.dubbo;

/**
 * @author wt.d
 * @date 15:10 2018/6/14
 */
public interface IGpHello {

    String sayHello(String msg);
}
