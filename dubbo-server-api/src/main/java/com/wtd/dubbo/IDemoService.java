package com.wtd.dubbo;

/**
 * @author wt.d
 * @date 17:48 2018/6/15
 */
public interface IDemoService {

    String protocolDemo(String msg);
}
