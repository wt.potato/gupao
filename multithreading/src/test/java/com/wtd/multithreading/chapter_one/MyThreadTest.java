package com.wtd.multithreading.chapter_one;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author wt.d
 * @date 13:50 2018/8/27
 */
public class MyThreadTest {

    @Test
    public void method1(){
        MyThread thread = new MyThread();
        thread.start();
        //为什么不会输出
        MyThread thread2 = new MyThread();
        thread2.start();
        System.out.println("state1:"+thread.getState());
        System.out.println("state2:"+thread2.getState());
        try {
            thread.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("state1:"+thread.getState());
        System.out.println("state2:"+thread2.getState());
    }

    @Test
    public void method2(){
        MyRunnable runnable = new MyRunnable();
        runnable.run();
        MyRunnable runnable2 = new MyRunnable();
        runnable2.run();
    }

}