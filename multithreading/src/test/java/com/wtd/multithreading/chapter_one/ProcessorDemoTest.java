package com.wtd.multithreading.chapter_one;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author wt.d
 * @date 15:09 2018/8/27
 */
public class ProcessorDemoTest {

    @Test
    public void  test(){
        Request request = new Request();
        request.setName("wtd");
        new ProcessorDemo().doTest(request);
    }

}