package com.wtd.multithreading.chapter_two;

import org.junit.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class ThreadSeqDemoTest {
    private static Set<String> set = new HashSet<>();
    private volatile static boolean stop = false;
    @Test
    public void test() throws InterruptedException {
        Thread t = new Thread(()->{
            while (!stop){
                try {
                    ThreadSeqDemo threadSeqDemo = new ThreadSeqDemo();
                    set.add(threadSeqDemo.seq());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        TimeUnit.SECONDS.sleep(10);
        stop = true;
        System.out.println(set.toString());
//        Iterator<String> iterator = set.iterator();
//        while (iterator.hasNext())
//            System.out.println(iterator.next());
//        }

    }

}