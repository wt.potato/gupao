package com.wtd.multithreading.chapter_four;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wt.d
 * @date 15:20 2018/8/28
 */
public class AtomicDemo {

    private static int count = 0;
    static Lock lock = new ReentrantLock();
    public static void inc() {
        lock.lock();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count++;
        lock.unlock();
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            new Thread(()->AtomicDemo.inc()).start();
        }
        Thread.sleep(1000 * 2);
        System.out.println("count=" + count);

    }

}
