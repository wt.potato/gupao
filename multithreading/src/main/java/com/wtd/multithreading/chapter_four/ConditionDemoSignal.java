package com.wtd.multithreading.chapter_four;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author wt.d
 * @date 14:14 2018/8/29
 */
public class ConditionDemoSignal implements Runnable{

    private Lock lock;
    private Condition condition;

    public ConditionDemoSignal(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        System.out.println("begin->ConditionDemoSignal");
        try {
            lock.lock();
            condition.signal();
            System.out.println("end->ConditionDemoSignal");
        } finally {
            lock.unlock();
        }
    }
}
