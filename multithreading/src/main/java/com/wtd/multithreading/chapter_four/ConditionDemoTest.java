package com.wtd.multithreading.chapter_four;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wt.d
 * @date 14:21 2018/8/29
 */
public class ConditionDemoTest {

    public static void main(String[] args) {
        //线程间通信
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        new Thread(()->{
            ConditionDemoWait wait = new ConditionDemoWait(lock, condition);
            wait.run();
        }).start();
        new Thread(()->{
            ConditionDemoSignal signal = new ConditionDemoSignal(lock, condition);
            signal.run();
        }).start();
    }
}
