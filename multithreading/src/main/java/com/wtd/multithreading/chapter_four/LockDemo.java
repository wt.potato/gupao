package com.wtd.multithreading.chapter_four;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author wt.d
 * @date 15:37 2018/8/28
 */
public class LockDemo {

    static Map<String, Object> cacheMap = new HashMap<>();
    static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    static Lock read = readWriteLock.readLock();
    static Lock write = readWriteLock.writeLock();

    public static final Object get(String key){
        read.lock();
        System.out.println("读-锁状态：" + read.tryLock());
        try {
            Thread.sleep(1000);
            System.out.println("开始读数据");
            return cacheMap.get(key);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            read.unlock();
        }
        return null;
    }

    public static final Object put(String key, Object value) {
        write.lock();
        try {
            Thread.sleep(1000);
            System.out.println("开始写数据");
            return cacheMap.put(key, value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            write.unlock();
        }
        return null;
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            System.out.println(LockDemo.put("1",1));
        });
        Thread t2 = new Thread(()->{
            System.out.println(LockDemo.put("2",2));
        });
        Thread t3 = new Thread(()->{
            System.out.println(LockDemo.get("1"));
        });
        Thread t4 = new Thread(()->{
            System.out.println(LockDemo.get("2"));
        });
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
    //读锁与读锁可以共享
    //读锁与写锁不可以共享（排他）
    //写锁与写锁不可以共享（排他）
}
