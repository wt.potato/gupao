package com.wtd.multithreading.chapter_one;

import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 16:20 2018/8/27
 */
public class InterruptDemo4 {

    private volatile static boolean stop = false;

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            int i = 0;
            while (!stop) {
                i++;
            }
            System.out.println("num:" + i);
        }, "InterruptDemo4");
        thread.start();
        Thread.sleep(1000);
        stop = true;
    }
}
