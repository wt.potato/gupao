package com.wtd.multithreading.chapter_one;

/**
 * @author wt.d
 * @date 14:48 2018/8/27
 */
public interface RequestProcessor {

    void processorRequest(Request request);
}
