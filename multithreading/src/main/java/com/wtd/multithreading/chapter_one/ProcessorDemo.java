package com.wtd.multithreading.chapter_one;

/**
 * @author wt.d
 * @date 15:02 2018/8/27
 */
public class ProcessorDemo {
    PrintProcessor printProcessor;

    protected ProcessorDemo() {
        SaveProcessor saveProcessor = new SaveProcessor();
        saveProcessor.start();
        printProcessor = new PrintProcessor(saveProcessor);
        printProcessor.start();
    }

    public void doTest(Request request){
        printProcessor.processorRequest(request);
    }

}
