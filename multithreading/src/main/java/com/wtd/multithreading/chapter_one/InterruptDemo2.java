package com.wtd.multithreading.chapter_one;

import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 16:20 2018/8/27
 */
public class InterruptDemo2 {
    private static int i;
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            while (true) {
                boolean ii = Thread.currentThread().isInterrupted();
                if (ii) {
                    System.out.println("before:" + ii);
                    Thread.interrupted();//设置中断标识，中断标识为false
                    System.out.println("after:" + Thread.currentThread().isInterrupted());
                }
//                System.out.println("Num:" + i++);
            }
        },"InterruptDemo2");
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("init:"+Thread.currentThread().isInterrupted());
        thread.interrupt();//设置中断标识，中断标识为TRUE
    }
}
