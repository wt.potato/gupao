package com.wtd.multithreading.chapter_one;

/**
 * @author wt.d
 * @date 11:34 2018/8/27
 */
public class MyThread extends Thread{

    @Override
    public void run() {
        System.out.println("MyThread run");
    }

}
