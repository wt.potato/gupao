package com.wtd.multithreading.chapter_one;

/**
 * @author wt.d
 * @date 14:45 2018/8/27
 */
public class Request {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Request{" +
                "name='" + name + '\'' +
                '}';
    }
}
