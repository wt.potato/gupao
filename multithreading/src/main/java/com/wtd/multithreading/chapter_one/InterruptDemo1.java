package com.wtd.multithreading.chapter_one;

import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 16:20 2018/8/27
 */
public class InterruptDemo1 {

    private static int i;
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            while (!Thread.currentThread().isInterrupted()) {
                i++;
            }
            System.out.println("Num:" + i);
        },"InterruptDemo1");
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("init:"+Thread.currentThread().isInterrupted());
        thread.interrupt();//设置中断标识，中断标识为TRUE
    }
}
