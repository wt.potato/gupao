package com.wtd.multithreading.chapter_one;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author wt.d
 * @date 14:58 2018/8/27
 */
public class SaveProcessor extends Thread implements RequestProcessor {

    LinkedBlockingQueue<Request> requests = new LinkedBlockingQueue<>();
    @Override
    public void run() {
        while (true) {
            try {
                Request request = requests.take();
                System.out.println("begin save request info:" + request);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void processorRequest(Request request) {
        requests.add(request);
    }
}
