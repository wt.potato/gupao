package com.wtd.multithreading.chapter_one;

import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 15:39 2018/8/27
 */
public class ThreadStatus {

    public static void main(String[] args) {
        //TIME_WAITING
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"TimeWaiting").start();

        //WAITING，线程在ThreadStatus类锁上通过wait进行等待
        new Thread(()->{
            synchronized (ThreadStatus.class) {
                try {
                    ThreadStatus.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"Waiting").start();

        //线程在ThreaStatus加锁后，不会释放锁
        new Thread(new BlockDemo(), "BlockDemo-01").start();
        new Thread(new BlockDemo(), "BlockDemo-02").start();
    }

    static class BlockDemo extends Thread{
        @Override
        public void run() {
            synchronized (BlockDemo.class) {
                while (true) {
                    try {
                        TimeUnit.SECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    //jps  jstack pid >threadstatus.txt
}
