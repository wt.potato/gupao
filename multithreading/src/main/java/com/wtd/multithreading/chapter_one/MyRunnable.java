package com.wtd.multithreading.chapter_one;

/**
 * @author wt.d
 * @date 14:02 2018/8/27
 */
public class MyRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("MyRunnable run");
    }
}
