package com.wtd.multithreading.chapter_one;

import java.util.concurrent.TimeUnit;

/**
 * @author wt.d
 * @date 16:20 2018/8/27
 */
public class InterruptDemo3 {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            while (true) {
                try {
                    Thread.sleep(1000 * 10);
                } catch (InterruptedException e) {//该异常会导致线程复位
                    e.printStackTrace();
                }
            }
        },"InterruptDemo3");
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("init:"+Thread.currentThread().isInterrupted());
        thread.interrupt();//设置中断标识，中断标识为TRUE
        System.out.println("set:"+Thread.currentThread().isInterrupted());
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Exception:"+Thread.currentThread().isInterrupted());
    }
}
