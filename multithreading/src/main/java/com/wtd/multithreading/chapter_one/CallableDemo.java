package com.wtd.multithreading.chapter_one;

import java.util.concurrent.*;

/**
 * @author wt.d
 * @date 14:17 2018/8/27
 */
public class CallableDemo implements Callable<String> {

    @Override
    public String call() throws Exception {
        int a = 1;
        int b = 2;
        System.out.println(a + b);
        return "执行结果:" + (a + b);
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(1);
        CallableDemo callableDemo = new CallableDemo();
        Future<String> future =  service.submit(callableDemo);
        System.out.println(future.get());
        service.shutdown();
    }
}
