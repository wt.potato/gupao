package com.wtd.multithreading.chapter_three;

import java.util.concurrent.locks.Lock;

/**
 * @author wt.d
 * @date 11:20 2018/8/28
 */
public class WaitNotifyTest {

    public static void main(String[] args) {
        Object lock = new Object();
        ThreadWait wait = new ThreadWait(lock);
        ThreadNotify notify = new ThreadNotify(lock);
        wait.start();
        notify.start();
    }
}
