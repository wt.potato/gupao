package com.wtd.multithreading.chapter_three;

/**
 * @author wt.d
 * @date 11:03 2018/8/28
 */
public class SynchronizedDemo {

    private static int count = 0;

    public static void inc(){
        synchronized (SynchronizedDemo.class) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count++;
        }
    }
    public synchronized void inc2(){
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count++;
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            new Thread(()->
                    SynchronizedDemo.inc()
            ).start();
        }
        for (int i = 0; i < 1000; i++) {
            new Thread(()->
                    new SynchronizedDemo().inc2()
            ).start();
        }

        Thread.sleep(1000 * 5);

        System.out.println("count=" + count);
    }
}
